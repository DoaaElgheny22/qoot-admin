
import { Injectable } from '@angular/core';
import { WebApiService } from "../webApi.service";
import { HttpParams } from '@angular/common/http';

@Injectable()
export class ReportsService {
    private controller: string = '/Report';
    constructor(private webApi: WebApiService) { }
    
    GetListCategort(myparam) {
        return this.webApi.get(`${this.controller}/Category/GetList?city_id=`+myparam.city_id+'&chief_id'+myparam.chief_id+'&category_id='+myparam.category_id+'&StartDate='+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page);
    }
    GetListDataCategort(){
        return this.webApi.get(`${this.controller}/Category/Data`);
    }
    ExportCategory(myparam){
        return this.webApi.get(`${this.controller}/Category/Export?city_id=`+myparam.city_id+'&chief_id='+myparam.chief_id+'&category_id='+myparam.category_id+'&StartDate='+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page);
    }
    // Chieff
    GetListChief(myparam) {
        return this.webApi.get(`${this.controller}/Chief/GetList?city_id=`+myparam.city_id+'&StartDate='+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&status='+myparam.status);
    }
    GetListDataChief(){
        return this.webApi.get(`${this.controller}/Chief/Data`);
    }
    ExportChief(myparam){
        return this.webApi.get(`${this.controller}/Chief/Export?city_id=`+myparam.city_id+'&StartDate='+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&status='+myparam.status);
    }

    //Dish 
     
    GetListDish(myparam) {
        return this.webApi.get(`${this.controller}/Dish/GetList?city_id=`+myparam.city_id+'&chief_id='+myparam.chief_id+'&category_id='+myparam.category_id+'&StartDate='+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&approved='+myparam.approved);
    }
    GetListDataDish(){
        return this.webApi.get(`${this.controller}/Dish/Data`);
    }
    ExportDish(myparam){
        return this.webApi.get(`${this.controller}/Dish/Export?city_id=`+myparam.city_id+'&chief_id='+myparam.chief_id+'&category_id='+myparam.category_id+'&StartDate='+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&approved='+myparam.approved);
    }

    //customer
    GetListCustomer(myparam) {
        return this.webApi.get(`${this.controller}/Customer/GetList?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&status='+myparam.status);
    }
    GetListDataCustomer(){
        return this.webApi.get(`${this.controller}/Customer/Data`);
    }
    ExportCustomer(myparam){
        return this.webApi.get(`${this.controller}/Customer/Export?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&status='+myparam.status);
    }


 //order
 GetListOrder(myparam) {
    return this.webApi.get(`${this.controller}/Order/GetList?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&customer_id='+myparam.customer_id+'&chief_id='+myparam.chief_id+'&payment_method_id='+myparam.payment_method_id);
}
GetListDataOrder(){
    return this.webApi.get(`${this.controller}/Order/Data`);
}
ExportOrder(myparam){
    return this.webApi.get(`${this.controller}/Order/Export?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&customer_id='+myparam.customer_id+'&chief_id='+myparam.chief_id+'&payment_method_id='+myparam.payment_method_id);
}

GetListTransactionHistory(myparam) {
    return this.webApi.get(`${this.controller}/TransactionHistory/GetList?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&customer_id='+myparam.customer_id+'&chief_id='+myparam.chief_id+'&payment_method_id='+myparam.payment_method_id);
}

ExportTransactionHistory(myparam){
    return this.webApi.get(`${this.controller}/TransactionHistory/Export?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&customer_id='+myparam.customer_id+'&chief_id='+myparam.chief_id+'&payment_method_id='+myparam.payment_method_id);
}
//transaction history

DownloadFile(){
    return this.webApi.get(`${this.controller}/SendTransaction/DownloadFile`);
}
Import(myparam){
    return this.webApi.post(`${this.controller}/SendTransaction/Import`,myparam);
}
//QootIncome
GetListQootIncome(myparam) {
    return this.webApi.get(`${this.controller}/QootIncome/GetList?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&chief_id='+myparam.chief_id);
}
GetListDataQootIncome(){
    return this.webApi.get(`${this.controller}/QootIncome/Data`);
}
ExportQootIncome(myparam){
    return this.webApi.get(`${this.controller}/QootIncome/Export?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&chief_id='+myparam.chief_id);
}

//PaymentStatus
GetListPaymentStatus(myparam) {
    return this.webApi.get(`${this.controller}/PaymentStatus/GetList?chief_id=`+myparam.chief_id+'&page='+myparam.current_page+'&per_page='+myparam.per_page);
}
ExportPaymentStatus(myparam){
    return this.webApi.get(`${this.controller}/PaymentStatus/Export?chief_id=`+myparam.chief_id+'&page='+myparam.current_page+'&per_page='+myparam.per_page);
}

VisitorData(){
    return this.webApi.get(`${this.controller}/Visitor/Data`);
}
VisitorGetList(myparam){
    return this.webApi.get(`${this.controller}/Visitor/GetList?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&orderBy='+myparam.orderBy+'&isAscending='+myparam.isAscending);
}
ExportVisitor(myparam){
    return this.webApi.get(`${this.controller}/Visitor/Export?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.current_page+'&per_page='+myparam.per_page+'&orderBy='+myparam.orderBy+'&isAscending='+myparam.isAscending);
}
    //Get all City
    GetallCity() {
        return this.webApi.getFrommainsite(`/City/GetList`);
    }
    //Get all Category
    GetallCategory() {
        return this.webApi.getFrommainsite(`/Category/GetList`);
    }
    
  
}
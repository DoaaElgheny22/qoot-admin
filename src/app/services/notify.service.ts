import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

export interface Notify {
  type: NotifyTypes
  message: string
  _close?(index: number): void
  _class?: string
}
export enum NotifyTypes {
  Success = 'success',
  Info = 'info',
  Warning = 'warning',
  Error = 'error',
}
@Injectable()
export class NotifyService {

  private event: Subject<Notify>;

  constructor() {
    this.event = new Subject<Notify>();
  }

  private broadcast(tost: Notify) {
    this.event.next(tost);
  }

  on(): Observable<any> {
    return this.event.asObservable();//.filter(event => event.key === key).map(event => <T>event.data);
  }
  success(message): void {
    this.broadcast({ type: NotifyTypes.Success, message: message })
  }
  info(message): void {
    this.broadcast({ type: NotifyTypes.Info, message: message })
  }
  warning(message): void {
    this.broadcast({ type: NotifyTypes.Warning, message: message })
  }
  error(message): void {
    this.broadcast({ type: NotifyTypes.Error, message: message })
  }
}
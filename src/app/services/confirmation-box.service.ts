import { Injectable } from '@angular/core';
import { ConfirmationBoxComponent } from '../sharedModules/confirmation-box/confirmation-box.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

interface Msg {
  ar: string;
  en: string;
}

@Injectable()
export class ConfirmationBoxService {
  constructor(private modelService: BsModalService) { }
  confirm(msg: Msg, confireBoxClose:any) {
    let confirmBox = this.modelService.show(ConfirmationBoxComponent, { initialState: { arMsg: msg.ar, enMsg: msg.en }, class: 'modal-sm' });
    confirmBox.content.confireBoxClose = (result) => {
      confirmBox.hide();
      confireBoxClose(result)
    };
  }
}
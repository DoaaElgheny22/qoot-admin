
import { Injectable } from '@angular/core';
import { WebApiService } from "../webApi.service";
import { HttpParams } from '@angular/common/http';

@Injectable()
export class DashBoardService {
    private controller: string = '/DashBoard';
    constructor(private webApi: WebApiService) { }
    
    OrderData(myparam) {
        return this.webApi.get(`${this.controller}/OrderData?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate);
    }

    ChiefData(myparam) {
        return this.webApi.get(`${this.controller}/ChiefData?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate);
    }

    PendingChief(myparam) {
      return this.webApi.get(`${this.controller}/PendingChief?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.page+'&orderBy='+myparam.orderBy+'&isAscending='+true+'&per_page='+1000);
    }

    NewDish(myparam) {
        return this.webApi.get(`${this.controller}/NewDish?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.page+'&orderBy='+myparam.orderBy+'&isAscending='+true+'&per_page='+1000);
    }
    NewCustomer(myparam) {
        return this.webApi.get(`${this.controller}/NewCustomer?StartDate=`+myparam.StartDate+'&EndDate='+myparam.EndDate+'&page='+myparam.page+'&orderBy='+myparam.orderBy+'&isAscending='+true+'&per_page='+1000);
    }

    AllChief() {
        return this.webApi.get(`${this.controller}/AllChief`);
    }
    
    Income(ChiefID) {
        return this.webApi.get(`${this.controller}/Income?ChiefID=`+ChiefID);
    }

    TopRateChiefs() {
        return this.webApi.get(`${this.controller}/TopRateChiefs`);
    }
    
    TopOrderChiefs() {
        return this.webApi.get(`${this.controller}/TopOrderChiefs`);
    }
}
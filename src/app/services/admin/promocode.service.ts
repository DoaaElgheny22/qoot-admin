
import { Injectable } from '@angular/core';
import { WebApiService } from "../webApi.service";
import { HttpParams } from '@angular/common/http';

@Injectable()
export class PromotionService {
    private controller: string = '/Promotion';
    constructor(private webApi: WebApiService) { }
    GetList(myparam) {
        return this.webApi.get(`${this.controller}/GetList?page=`+myparam.current_page+'&orderBy='+myparam.orderBy+'&isAscending='+myparam.isAscending+'&per_page='+myparam.per_page+'&sortBy='+myparam.sortBy+'&name='+myparam.name);
    }
    Post(myparam) {
        return this.webApi.post(`${this.controller}/Post`, myparam);
    }
    Update(myparam) {
        return this.webApi.post(`${this.controller}/Update`, myparam);
    }
    GetByID(ID){
        return this.webApi.get(`${this.controller}/GetByID/`+ID)
    }
    checkPromoCode(code){
        return this.webApi.post(`${this.controller}/checkPromoCode`,{promo_code:code}); 
    }
    Delete(ID){
        return this.webApi.get(`${this.controller}/Delete/`+ID); 
    }
}
import { Injectable } from '@angular/core';
import { hubConnection } from 'signalr-no-jquery';

@Injectable()
export class SignalRService {
    private connection: any;
    private hubProxy: any;
    constructor() { }
    initConnection(api_url: string): void {
        this.connection = hubConnection(api_url);
        this.hubProxy = this.connection.createHubProxy('ERPHub');
        this.connection.start({ jsonp: true, withCredentials: false })
            .done(() => { console.log(`SignalR connected, connection ID=${this.connection.id}`); })
            .fail(() => { console.warn('SignalR can not connect.'); });
    }
    on(key: string, callBack: Function) {
        this.hubProxy.on(key, (obj: any) => callBack(obj));
    }
}

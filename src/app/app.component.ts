

import { Component, OnDestroy, HostListener, OnInit } from '@angular/core';
import { SignalRService } from './services/signalr.service';
import { environment } from '../environments/environment';
import { AuthService } from './services/user/auth.service';
// import { environment } from '../../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit {
  title = 'qoot-admin';
  public dtOptions: DataTables.Settings = {};
  constructor(
    private authService: AuthService,
  ) { }
  ngOnDestroy() {
    this.authService.LogOut();
  }
  ngOnInit() {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };
  }
}


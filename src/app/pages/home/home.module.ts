import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { LoginComponent } from './login/login.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NotifyModule } from 'app/sharedModules/notify/notify.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from 'app/services/user/auth.service';
import { SharedModule } from 'app/sharedModules/shared.module';
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/login/', '.json');
}

@NgModule({
  imports: [
    //CommonModule,
    NotifyModule,
    //ReactiveFormsModule,
    SharedModule,
    HomeRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    AuthService
  ],
  declarations: [LoginComponent]
})
export class HomeModule { }

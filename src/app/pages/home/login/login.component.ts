import { Component, OnInit } from '@angular/core';
import { NotifyService } from '../../../services/notify.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../../services/user/auth.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  form: FormGroup;
  loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private notifyService: NotifyService,
    private translateService: TranslateService,
    private localStorageService:LocalStorageService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initForm();
    // this.translateService.use('en');
  }

  initForm() {
    this.form = this.formBuilder.group({
      LoginField: [null, [Validators.required, Validators.minLength(3)]],
      Password: [null, [Validators.required, Validators.minLength(3)]],
    });
  }

  login() {
		if (this.form.valid) {
			this.loading = true;
			this.authService.Login(this.form.value).subscribe(res=>{
        if(res.Success){
            if (res.Data !=null ) {
              console.log(res.Data)
              this.notifyService.success(res.Message)
                this.localStorageService.set('accessToken', res.Data.token);
                this.localStorageService.set('currentUser', res.Data);
                this.localStorageService.set("permissions", res.Data.permissions);
                this.localStorageService.set("allpermissions", res.Data.allpermissions);
                if(res.Data.permissions.find(i=>i.id==9)){
                  this.router.navigate(['/admin','dashboard']);
                }
                else
                {
                  this.router.navigate([res.Data.permissions[0].route]);
                }
            }
        }
        else{
             this.notifyService.error(res.Message)  
        }
      })
		} else {
			for (let control in this.form.controls) {
				this.form.get(control).markAsDirty();
			}
		}
	}
}

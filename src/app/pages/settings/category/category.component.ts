
import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { LanguageService } from 'app/services/language.service';
import { DataTableDirective } from 'angular-datatables';
import { environment } from 'environments/environment';
import { NotifyService } from 'app/services/notify.service';
import { CategoryService } from 'app/services/settings/category.service';
import { ConfirmationBoxService } from 'app/services/confirmation-box.service';
import { AddCategoryComponent } from './add-category/add-category.component';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
 Data:any={};
 addEditModel:BsModalRef;
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'ID',
    isAscending: true 
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private categoryService:CategoryService ,
  private confirmationBoxService: ConfirmationBoxService,
  private notifyService: NotifyService,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
  this.showpage=true
   this.getData();
  }

  refresh(){
    this.getData();
  }

  getData(){
   this.categoryService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
        if (res.Data.data.length) {this.Data = res.Data;}
        this.showpage=false
      }
    })
  }
  addNew(ID=0,Data=null){
    this.addEditModel = this.modalService.show(AddCategoryComponent,{ initialState: 
      { id:ID,Data:Data}, class: 'modal-lg' })
      this.addEditModel.content.onClose  = (res)=>{
      this.getData();    
      }
  }
  view(status){
    this.pagingparamater.sortBy=status
    this.getData();
  }
 
 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }
 Delete(id) {
  this.confirmationBoxService.confirm({ ar: 'برجاء تاكيد حذف ؟', en: 'Confirm Delete??' }, isConfirmed => {
    if (isConfirmed) {
      this.categoryService.Delete(id).subscribe(
        res => {
          if (res.Success) {
            this.notifyService.success(res.Message);
            this.getData();
          } else {
            this.notifyService.error(res.Message);
          }
        }
      );
    }
  });
}
}





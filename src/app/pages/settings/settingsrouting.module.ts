import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from '../../sharedModules/layouts/layout.component';
import { DashboardComponent } from '../admin/dashboard/dashboard.component';
import { CitiesComponent } from './cities/cities.component';
import { CategoryComponent } from './category/category.component';
import { BrandComponent } from './brand/brand.component';
import { CusinesComponent } from './cusines/cusines.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { SettingsQootComponent } from './settings-qoot/settings-qoot.component';
const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'cities', component: CitiesComponent },
      { path: 'category', component: CategoryComponent },
      { path: 'brands', component: BrandComponent },
      { path: 'cusine', component:  CusinesComponent },
      { path: 'subscribe', component:  SubscribeComponent },
      { path: 'contact-us', component:  ContactUsComponent },
      { path: 'about-us', component:  AboutUsComponent },
      { path: 'settings-qoot', component:  SettingsQootComponent },
    ]
  }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }

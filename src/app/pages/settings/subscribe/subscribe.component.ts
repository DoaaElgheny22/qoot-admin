import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { LanguageService } from 'app/services/language.service';
import { DataTableDirective } from 'angular-datatables';
import { environment } from 'environments/environment';
import { NotifyService } from 'app/services/notify.service';
import { CategoryService } from 'app/services/settings/category.service';
import { SubscribeService } from 'app/services/settings/subscribe.service';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {
 Data:any={};
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'ID',
    isAscending: true 
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private subscribeService:SubscribeService ,
  private notifyService: NotifyService,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
  this.showpage=true
   this.getData();
  }

  refresh(){
    this.getData();
  }

  getData(){
   this.subscribeService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
        if (res.Data.data.length) {this.Data = res.Data;}
        this.showpage=false
      }
    })
  }
  
  view(status){
    this.pagingparamater.sortBy=status
    this.getData();
  }
 
 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }

}






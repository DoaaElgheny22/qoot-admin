import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SttingsEmailComponent } from './sttings-email.component';

describe('SttingsEmailComponent', () => {
  let component: SttingsEmailComponent;
  let fixture: ComponentFixture<SttingsEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SttingsEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SttingsEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

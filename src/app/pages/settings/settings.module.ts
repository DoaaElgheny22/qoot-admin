import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settingsrouting.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TreeModule } from 'ng2-tree';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from 'app/sharedModules/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { ModalModule } from 'ngx-bootstrap';
import { CitiesComponent } from './cities/cities.component';
import { CategoryComponent } from './category/category.component';
import { CusinesComponent } from './cusines/cusines.component';
import { CityService } from 'app/services/settings/cities.service';
import { CategoryService } from 'app/services/settings/category.service';
import { BrandService } from 'app/services/settings/brands.service';
import { BrandComponent } from './brand/brand.component';
import { CusineService } from 'app/services/settings/cuisines.service';
import { SubscribeService } from 'app/services/settings/subscribe.service';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { AddCityComponent } from './cities/add-city/add-city.component';
import { AddBrandComponent } from './brand/add-brand/add-brand.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import { AddCusinesComponent } from './cusines/add-cusines/add-cusines.component';
import { SettingsLayoutComponent } from './settings-layout/settings-layout.component';
import { ContactUsService } from 'app/services/settings/contactus.service';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { SettingsQootComponent } from './settings-qoot/settings-qoot.component';
import { AboutUSService } from 'app/services/settings/aboutus.service';
import { SettingsQootService } from 'app/services/settings/setting-qoot.service';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/Settings/', '.json');
}
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SettingsRoutingModule,
    ModalModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    DataTablesModule.forRoot(),
    TreeModule
  ],
  declarations: [
    CitiesComponent,
    CategoryComponent,
    CusinesComponent,
    BrandComponent,
    SubscribeComponent,
    AddCityComponent,
    AddBrandComponent,
    AddCategoryComponent,
    AddCusinesComponent,
    SettingsLayoutComponent,
    ContactUsComponent,
    AboutUsComponent,
    SettingsQootComponent
    
  ],
  providers: [
    CityService,
    CategoryService,
    BrandService,
    CusineService,
    SubscribeService,
    ContactUsService,
    AboutUSService,
    SettingsQootService
  ],
  entryComponents: [
    AddCityComponent,
    AddBrandComponent,
    AddCategoryComponent,
    AddCusinesComponent
  ]
})
export class SettingsModule { }

import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { LanguageService } from 'app/services/language.service';

@Component({
  selector: 'app-settings-layout',
  templateUrl: './settings-layout.component.html',
  styleUrls: ['./settings-layout.component.css']
})
export class SettingsLayoutComponent implements OnInit {
  tabactive:number;
settingsPermission:any=[];
Data:any=[]
@Input("mycurrenttab") mycurrenttab: any = null;
  constructor( private activeRoute: ActivatedRoute,
    public languageService: LanguageService,
               public localStorageService:LocalStorageService
    ) { }
  ngOnInit() {
  
  this.tabactive=this.mycurrenttab
  this.Data=this.localStorageService.get('allpermissions');
  this.settingsPermission= this.Data.filter(i=>i.parent_id==8 && i.type=='view');
  this.tabactive=this.settingsPermission[0].id
  }
  clickactive(id){
     console.log(id)
    this.tabactive=id;
  }

}

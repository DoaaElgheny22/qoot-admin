
import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { LanguageService } from 'app/services/language.service';
import { DataTableDirective } from 'angular-datatables';
import { NotifyService } from 'app/services/notify.service';
import { BrandService } from 'app/services/settings/brands.service';
import { ConfirmationBoxService } from 'app/services/confirmation-box.service';
import { ContactUsService } from 'app/services/settings/contactus.service';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
 Data:any={};
 addEditModel:BsModalRef;
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'ID',
    isAscending: true 
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private contactUsService:ContactUsService,
  private notifyService: NotifyService,
  private confirmationBoxService: ConfirmationBoxService,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
  this.showpage=true
   this.getData();
  }

  refresh(){
    this.getData();
  }

  getData(){
   this.contactUsService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
        console.log(res.Data,"lll")
        if (res.Data.data.length) {this.Data = res.Data;}
        this.showpage=false
      }
    })
  }
  view(status){
    this.pagingparamater.sortBy=status
    this.getData();
  }

 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }

}






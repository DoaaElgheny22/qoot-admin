import { Component, OnInit } from '@angular/core';
import { SettingsQootService } from 'app/services/settings/setting-qoot.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotifyService } from 'app/services/notify.service';

@Component({
  selector: 'app-settings-qoot',
  templateUrl: './settings-qoot.component.html',
  styleUrls: ['./settings-qoot.component.css']
})
export class SettingsQootComponent implements OnInit {
  showpage:boolean=false;
  form: FormGroup;
  Data:any=[];
  constructor(private settingsQootService:SettingsQootService,
    private notifyService: NotifyService,
    private formBuilder: FormBuilder, ) { }

  ngOnInit() {
    this.initform();
    this.getData();
  }
  getData(){
    this.settingsQootService.GetList().subscribe(
     res => {
       if (res.Success) {
        this.Data = res.Data;
          this.form.patchValue(res.Data);
          this.showpage=false;
       }
     })
   }
   initform() {
    this.form = this.formBuilder.group({
      id: [0],
      title:[null, Validators.required],
      facebook:[null, Validators.required],
      twitter:[null,Validators.required],
      instagram:[null,Validators.required],
      phone:[null,Validators.required],
      contactus_email:[null,Validators.required],
      email:[null,Validators.required],
      password:[null,Validators.required],
      qoot_percentage_weeklyDeal:[null,Validators.required],
      qoot_percentage_dish:[null,Validators.required],
      auto_reject_time:[null,Validators.required],
    });
  }

  
  save() {
    if (this.form.valid) {
       if ( this.form.dirty) {
        this.settingsQootService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      }
    }
    else {
      for (let control in this.form.controls) {
        this.form.get(control).markAsDirty();
      }
    }
  }
}

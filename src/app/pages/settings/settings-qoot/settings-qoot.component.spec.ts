import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsQootComponent } from './settings-qoot.component';

describe('SettingsQootComponent', () => {
  let component: SettingsQootComponent;
  let fixture: ComponentFixture<SettingsQootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsQootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsQootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

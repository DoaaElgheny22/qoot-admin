
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { NotifyService } from 'app/services/notify.service';
import { CityService } from 'app/services/settings/cities.service';
import { BrandService } from 'app/services/settings/brands.service';
import { HttpRequest, HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-add-brand',
  templateUrl: './add-brand.component.html',
  styleUrls: ['./add-brand.component.css']
})
export class AddBrandComponent implements OnInit {
  form: FormGroup;
  fileData=null;
  img:any;
  id:number;
  onClose: any;
  Data:any;
  constructor(private formBuilder: FormBuilder, 
    public myModel: BsModalRef,
    private http: HttpClient,
    public brandService:BrandService,
    private notifyService: NotifyService,
    ) { }
  ngOnInit() {
    this.initform()
    if(this.id>0){
    this.form.patchValue(this.Data);
    this.img=environment.api_img+this.Data.img
    }
  }
  onSelectFile(event,type) {
        if (event.target.files && event.target.files[0]) {
          var reader = new FileReader();
          reader.readAsDataURL(event.target.files[0]); // read file as data url
          this.fileData = <File>event.target.files[0];
          this.img=this.fileData
          reader.onload =(event:any) => {
              this.img=(event.target.result);
              this.uploadmyImage(this.fileData);
          }
    }}
    uploadmyImage(Data){
      const formData = new FormData();
      formData.append('img',Data);
      this.http.request(new HttpRequest('POST',`${environment.api_url_main}/UploadImage`,
      formData,{ reportProgress: true })).subscribe(
        event => {
          if (event.type === HttpEventType.Response) {
            if (event.body['Success']) {
                  this.form.get('img').setValue(event.body['Data'].image)
            } else {
              this.notifyService.error("something wrong upload again");
            }
          }
        },
      );
    }
initform() {
    this.form = this.formBuilder.group({
      id: [0],
      ar_name:[null, [Validators.required, Validators.pattern('^[\u0621-\u064A0-9 ]+$')]],
      en_name:[null, [Validators.required,Validators.pattern('[0-9A-Za-z ]+$')]],
      url:[null,Validators.required],
      img:[null]
    });
  }

  save() {
    if (this.form.valid) {
      if (this.form.value.id == 0) {
        this.brandService.Post(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      } else if (this.form.value.id > 0 && this.form.dirty) {
        this.brandService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      }
    }
    else {
      for (let control in this.form.controls) {
        this.form.get(control).markAsDirty();
      }
    }
  }
}


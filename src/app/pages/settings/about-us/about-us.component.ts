import { Component, OnInit } from '@angular/core';
import { NotifyService } from 'app/services/notify.service';
import { ConfirmationBoxService } from 'app/services/confirmation-box.service';
import { AboutUSService } from 'app/services/settings/aboutus.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  showpage:boolean=false;
  form: FormGroup;
  Data:any=[];
  constructor( 
    private aboutUSService:AboutUSService,
    private notifyService: NotifyService,
    private formBuilder: FormBuilder,
    private confirmationBoxService: ConfirmationBoxService,) { }

  ngOnInit() {
    this.getData();
    this.initform();
  }
  getData(){
    this.showpage=true
    this.aboutUSService.GetList().subscribe(
     res => {
       if (res.Success) {
      
           this.Data = res.Data;
           this.form.patchValue(res.Data);
           this.showpage=false;
          
       
       }
     })
   }
   initform() {
    this.form = this.formBuilder.group({
      id:[null],
      ar_text_1:[null],
      en_text_1:[null],

      ar_text_about:[null],
      en_text_about:[null],

      ar_text_location:[null],
      en_text_location:[null],

      ar_text_search:[null],
      en_text_search:[null],

      ar_text_order:[null],
      en_text_order:[null],

      ar_text_enjoy:[null],
      en_text_enjoy:[null]

    });
  }

  save() {
    // debugger
    // if (this.form.valid) {
    //    if ( this.form.dirty) {
        this.aboutUSService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      }
    // }
    // else {
    //   for (let control in this.form.controls) {
    //     this.form.get(control).markAsDirty();
    //   }
    // }
//  }

}

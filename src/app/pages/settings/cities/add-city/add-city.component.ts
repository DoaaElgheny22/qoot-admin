import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { NotifyService } from 'app/services/notify.service';
import { CityService } from 'app/services/settings/cities.service';
@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.css']
})
export class AddCityComponent implements OnInit {
  form: FormGroup;
  id:number;
  onClose: any;
  Data:any;
  constructor(private formBuilder: FormBuilder, 
    public myModel: BsModalRef,
    public cityService:CityService,
    private notifyService: NotifyService,
    ) { }
  ngOnInit() {
    this.initform()
    if(this.id>0){
    this.form.patchValue(this.Data);
    }
  }
initform() {
    this.form = this.formBuilder.group({
      id: [0],
      ar_name:[null, [Validators.required, Validators.pattern('^[\u0621-\u064A0-9 ]+$')]],
      en_name:[null, [Validators.required,Validators.pattern('[0-9A-Za-z ]+$')]],
      delivery_fee:[0,Validators.required],
      isactive: [0, Validators.required]
    });
  }

  save() {
    if (this.form.valid) {
      if (this.form.value.id == 0) {
        this.cityService.Post(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      } else if (this.form.value.id > 0 && this.form.dirty) {
        this.cityService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      }
    }
    else {
       
      for (let control in this.form.controls) {
        this.form.get(control).markAsDirty();
      }
// to make it 

    }
  }
}

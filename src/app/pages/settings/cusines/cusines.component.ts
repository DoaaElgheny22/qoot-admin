import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { LanguageService } from 'app/services/language.service';
import { DataTableDirective } from 'angular-datatables';
import { environment } from 'environments/environment';
import { NotifyService } from 'app/services/notify.service';
import { CusineService } from 'app/services/settings/cuisines.service';
import { ConfirmationBoxService } from 'app/services/confirmation-box.service';
import { AddCusinesComponent } from './add-cusines/add-cusines.component';
@Component({
  selector: 'app-cusines',
  templateUrl: './cusines.component.html',
  styleUrls: ['./cusines.component.css']
})
export class CusinesComponent implements OnInit {
 Data:any={};
 addEditModel:BsModalRef;
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'ID',
    isAscending: true 
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private cusineService:CusineService ,
  private confirmationBoxService: ConfirmationBoxService,
  private notifyService: NotifyService,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
  this.showpage=true
   this.getData();
  }

  refresh(){
    this.getData();
  }

  getData(){
   this.cusineService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
        if (res.Data.data.length) {this.Data = res.Data;}
        this.showpage=false
      }
    })
  }
  
  view(status){
    this.pagingparamater.sortBy=status
    this.getData();
  }
  addNew(ID=0,Data=null){
    this.addEditModel = this.modalService.show(AddCusinesComponent,{ initialState: 
      { id:ID,Data:Data}, class: 'modal-lg' })
      this.addEditModel.content.onClose  = (res)=>{
      this.getData();    
      }
  }
 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }
 Delete(id) {
  this.confirmationBoxService.confirm({ ar: 'برجاء تاكيد حذف ؟', en: 'Confirm Delete??' }, isConfirmed => {
    if (isConfirmed) {
      this.cusineService.Delete(id).subscribe(
        res => {
          if (res.Success) {
            this.notifyService.success(res.Message);
            this.getData();
          } else {
            this.notifyService.error(res.Message);
          }
        }
      );
    }
  });
 }
}






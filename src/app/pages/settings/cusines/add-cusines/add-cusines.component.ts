import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { NotifyService } from 'app/services/notify.service';
import { CityService } from 'app/services/settings/cities.service';
import { CusineService } from 'app/services/settings/cuisines.service';
@Component({
  selector: 'app-add-cusines',
  templateUrl: './add-cusines.component.html',
  styleUrls: ['./add-cusines.component.css']
})
export class AddCusinesComponent implements OnInit {
  form: FormGroup;
  id:number;
  onClose: any;
  Data:any;
  constructor(private formBuilder: FormBuilder, 
    public myModel: BsModalRef,
    public cusineService:CusineService,
    private notifyService: NotifyService,
    ) { }
  ngOnInit() {
    this.initform()
    if(this.id>0){
    this.form.patchValue(this.Data);
    }
  }
initform() {
    this.form = this.formBuilder.group({
      id: [0],
      ar_name:[null, [Validators.required, Validators.pattern('^[\u0621-\u064A0-9 ]+$')]],
      en_name:[null, [Validators.required,Validators.pattern('[0-9A-Za-z ]+$')]],
    });
  }

  save() {
    if (this.form.valid) {
      if (this.form.value.id == 0) {
        this.cusineService.Post(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      } else if (this.form.value.id > 0 && this.form.dirty) {
        this.cusineService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      }
    }
    else {
      for (let control in this.form.controls) {
        this.form.get(control).markAsDirty();
      }
    }
  }
}


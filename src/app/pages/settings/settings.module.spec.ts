
import { SettingsRoutingModule } from './settingsrouting.module';
import { SettingsModule } from './settings.module';

describe('settingsModule', () => {
  let settingsModule: SettingsRoutingModule;

  beforeEach(() => {
    settingsModule = new SettingsModule();
  });

  it('should create an instance', () => {
    expect(settingsModule).toBeTruthy();
  });
});

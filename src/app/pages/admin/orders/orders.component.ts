
import { Component, OnInit } from '@angular/core';
import { LanguageService } from 'app/services/language.service';
import { DataTableDirective } from 'angular-datatables';
import { NotifyService } from 'app/services/notify.service';
import { OrderService } from 'app/services/admin/order.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { environment } from 'environments/environment';
import { LocalStorageService } from 'angular-2-local-storage';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
 Data:any={};
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'date',
    isAscending: false 
  };
  dtOptions:any;
  addEditModel:BsModalRef;
  loading:boolean=false;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private orderService:OrderService,
  private notifyService: NotifyService,
  private localStorageService: LocalStorageService,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
  this.showpage=true
   this.getData();
   this.orderService.GetListData().subscribe(res=>{this.totalData=res.Data;
    this.showpage=false
    })
  }
  orderDetails(ID){
    this.addEditModel = this.modalService.show(OrderDetailComponent,{ initialState: 
      { ID:ID}, class: 'modal-lg' })
      this.addEditModel.content.onClose  = (res)=>{
      this.getData();    
      }
  }
  refresh(){
    this.getData();
  }

  getData(){
   this.orderService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
         this.Data = res.Data;
      }
    })
  }
  
  view(status){
    this.pagingparamater.sortBy=status
    this.getData();
  }
  Export() {
    this.orderService.Export().subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }
  showInvoice(id,Data){
    console.log(Data)
    this.localStorageService.remove('Bill-recept');
    this.localStorageService.set('Bill-recept',Data);
    const dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
    const dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

    const width  = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    const left = ((width / 2) - (800 / 2)) + dualScreenLeft;
    const top  = ((height / 2) - (600 / 2)) + dualScreenTop;

    const href = '#/admin/orders/' + id;
    const thisWindow = window.open(href, 'random', `scrollbars=yes, width=800, height=600, top=${top}, left=${left}`);
    thisWindow.onload = () => {
            thisWindow.print();
            thisWindow.close();
          };
  }
 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }

}



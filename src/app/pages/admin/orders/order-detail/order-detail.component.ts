import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { OrderService } from 'app/services/admin/order.service';
import { LanguageService } from 'app/services/language.service';
import { NotifyService } from 'app/services/notify.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
 ID:any;
 Data:any;
 onClose: any;
 myparam:any
 loading:boolean=true;
 NetTotal=0;
  constructor(private modalService: BsModalService,
              public myModel: BsModalRef,
              private notifyService: NotifyService,
              private languageService:LanguageService,
              private orderService:OrderService,) { }

  ngOnInit() {
   this.getOrder();
  }
  getOrder(){
    this.loading=true;
    this.orderService.GetByID(this.ID).subscribe(res=>{
      this.Data=res.Data;
      this.loading=false;
      this.NetTotal=this.Data.NetTotal+this.Data.delivery_fee;
    })
  }
  
  UpdateStatus(status){
    this.myparam={OrderID:this.Data.id,status:status}
    this.orderService.UpdateStatus(this.myparam).subscribe(res=>{this.notifyService.success(res.Message);
      this.myModel.hide();
      this.onClose();
    })

  }
}

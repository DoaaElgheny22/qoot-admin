import { Component, OnInit } from '@angular/core';
import * as moment from 'moment-timezone';
import { OrderService } from 'app/services/admin/order.service';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { LanguageService } from 'src/app/services/language.service';
@Component({
  selector: 'app-invoice-recipt',
  templateUrl: './invoice-recipt.component.html',
  styleUrls: ['./invoice-recipt.component.css']
})
export class InvoiceReciptComponent implements OnInit {
  myData: any;
  Date: any;
  loading = false;
  constructor(private oderservice: OrderService,
              private activeRoute: ActivatedRoute,
              public languageService: LanguageService,
              private localStorageService: LocalStorageService, ) { }

  ngOnInit() {
    this.myData = this.localStorageService.get('Bill-recept');
    console.log( this.myData.code ,"kkk");
   this.oderservice.GetByID(+this.activeRoute.snapshot.paramMap.get('id'))
    .subscribe(res => {
     this.myData = res.Data; 
   });
   this.Date = moment().format('YYYY-MM-DD hh:mm:ss A');
  }

}

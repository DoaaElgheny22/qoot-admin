import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from '../../sharedModules/layouts/layout.component';
import { DashboardComponent } from '../admin/dashboard/dashboard.component';
import { StaffMangmentComponent } from './staff-mangment/staff-mangment.component';
import { OrdersComponent } from './orders/orders.component';
import { KitchensComponent } from './kitchens/kitchens.component';
import { DishesComponent } from './dishes/dishes.component';
import { CustomersComponent } from './customers/customers.component';
import { ShowProfileComponent } from './kitchens/show-profile/show-profile.component';
import { ShowCustomerComponent } from './customers/show-customer/show-customer.component';
import { RolesComponent } from './roles/roles.component';
import { AddRoleComponent } from './roles/add-role/add-role.component';
import { PromocodeComponent } from './promocode/promocode.component';
import { ShowAdminProfileComponent } from './show-admin-profile/show-admin-profile.component';
import { InvoiceReciptComponent } from './orders/invoice-recipt/invoice-recipt.component';
const routes: Routes = [
  {
    path: 'orders/:id',
    component: InvoiceReciptComponent,
  },

  {
    path: '',
    component: LayoutComponent,
    children:
     [

      { path: 'dashboard', component: DashboardComponent },
      { path: 'customers', component: CustomersComponent },
      { path: 'dishes', component: DishesComponent },
      { path: 'kitchens', component: KitchensComponent },
      { path: 'show-profile/:id', component: ShowProfileComponent },
      { path: 'show-customer/:id', component: ShowCustomerComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'staff-mangment', component: StaffMangmentComponent },
      { path: 'role-mangment/:id', component: AddRoleComponent },
      { path: 'role-mangment', component: RolesComponent },
      { path: 'promo-code', component: PromocodeComponent },
      {path:'show-admin-profile/:id',component:ShowAdminProfileComponent}
      ]
  }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { RolesService } from 'app/services/admin/role.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'app/services/language.service';
import { NotifyService } from 'app/services/notify.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.css']
})
export class AddRoleComponent implements OnInit {
  permission:any=[];
  loading:boolean=false;
  permissionEdit:any=[];
  RolebyId:any;
  permissionSelect:any=[];
  form: FormGroup;
  constructor( private rolesService:RolesService,
               private router: Router,
               private activeRoute: ActivatedRoute,
               private formBuilder: FormBuilder, 
               private notifyService: NotifyService,
               private languageService:LanguageService
    ) { }

  ngOnInit() {
    this.initform();
    this.getData();
  }
  getData(){
    this.loading=true;
    if(+this.activeRoute.snapshot.paramMap.get('id')>0){
      this.rolesService.GetByID(+this.activeRoute.snapshot.paramMap.get('id')).subscribe(myres=>{
        this.RolebyId=myres.Data;
        this.form.patchValue(this.RolebyId);
        this.form.get('RoleID').setValue(this.RolebyId.id);
        this.rolesService.Permission().subscribe(res=>{
          res.Data.map(i=>i.childrens.map(i=>i.active=false))
        this.RolebyId.permissions.forEach(element => {
        this.permissionSelect.push(element.id);
        this.form.get('permissions').setValue(this.permissionSelect);
          //check
          let ExsitID= res.Data.find(i=>i.childrens.find(i=>i.id==element.id));
        if(ExsitID.childrens.find(i=>i.id==element.id).id==element.id){
          res.Data.find(i=>i.id==ExsitID.id).childrens.filter(i=>i.id==element.id).map(i=>i.active=true)}
        });
        this.loading=false;
        this.permission=res.Data;
      })
    })
  }
    else{
      this.rolesService.Permission().subscribe(res=>{
        res.Data.map(i=>i.childrens.map(i=>i.active=false))
        this.permission=res.Data;
      })
    }
  }

  permissionadd(e,id){
    if(this.permissionSelect.length==0){
      this.permissionSelect.push(id);
      this.form.get('permissions').setValue(this.permissionSelect)
    }
    else{
      let index =this.permissionSelect.indexOf(id);
      if(index==-1){
        this.permissionSelect.push(id);
        this.form.get('permissions').setValue(this.permissionSelect)
      }
      else{
        this.permissionSelect.splice(index,1);
       this.form.get('permissions').setValue(this.permissionSelect)
      }
    }
  }

  initform() {
    this.form = this.formBuilder.group({
      RoleID: [0],
      permissions:[],
      ar_name:[null, Validators.required],
      en_name:[null, Validators.required],
    });
  }

  save() {
    if(this.form.value.permissions==null){
      this.notifyService.error('يجب ان تختار علي الاقل اذن واحد');
    }
    else if(this.form.value.permissions.length==0){
      this.notifyService.error('يجب ان تختار علي الاقل اذن واحد');
    }
    else if (this.form.valid) {
      if (this.form.value.RoleID == 0) {
        this.rolesService.Post(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.router.navigate(['/admin','role-mangment']);
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      } else if (this.form.value.RoleID > 0 ) {
        this.rolesService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.router.navigate(['/admin','role-mangment']);
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      }
    }
    else {
      for (let control in this.form.controls) {
        this.form.get(control).markAsDirty();
      }

    }
  }
}

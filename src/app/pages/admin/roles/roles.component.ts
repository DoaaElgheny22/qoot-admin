import { Component, OnInit } from '@angular/core';
import { RolesService } from 'app/services/admin/role.service';
import { ConfirmationBoxService } from 'app/services/confirmation-box.service';
import { NotifyService } from 'app/services/notify.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  roles:any=[];
  loading:boolean=false;
  constructor(
    private rolesService:RolesService,
    private notifyService: NotifyService,
    private confirmationBoxService: ConfirmationBoxService,
    ) { }

  ngOnInit() {
    this.getData();
  }
  getData(){
    this.loading=true
    this.rolesService.GetList().subscribe(res=>{this.roles=res.Data;this.loading=false})
  }
  Delete(id) {
    this.confirmationBoxService.confirm({ ar: 'برجاء تاكيد حذف ؟', en: 'Confirm Delete??' }, isConfirmed => {
      if (isConfirmed) {
        this.rolesService.Delete(id).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.getData();
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      }
    });
  }

}

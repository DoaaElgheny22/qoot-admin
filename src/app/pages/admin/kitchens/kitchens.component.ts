import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { LanguageService } from 'app/services/language.service';
import { KitchenService } from 'app/services/admin/kitchen.service';
import { DataTableDirective } from 'angular-datatables';
import { environment } from 'environments/environment';
import { NotifyService } from 'app/services/notify.service';

@Component({
  selector: 'app-kitchens',
  templateUrl: './kitchens.component.html',
  styleUrls: ['./kitchens.component.css']
})
export class KitchensComponent implements OnInit {
 Data:any={};
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'ID',
    isAscending: 'DESC' 
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private kitchenService:KitchenService,
  private notifyService: NotifyService,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
  this.showpage=true
   this.getData();
   this.kitchenService.GetListData().subscribe(res=>{this.totalData=res.Data;
    this.showpage=false
    })
  }
  Export() {
    this.kitchenService.Export().subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }
  getData(){
   this.kitchenService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
      this.Data = res.Data;
      }
    })
  }
  
  view(status){
    this.pagingparamater.sortBy=status
    this.getData();
  }

  approve(ID){
     this.kitchenService.Approve(ID).subscribe(res=>{
       if(res.Success){
        this.notifyService.success(res.Message);
       }
       else{
        this.notifyService.error(res.Message);
       }
      }) 
  }
 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }

}

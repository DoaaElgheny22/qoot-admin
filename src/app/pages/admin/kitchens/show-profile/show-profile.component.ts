import { Component, OnInit } from '@angular/core';
import { KitchenService } from 'app/services/admin/kitchen.service';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'app/services/language.service';
import { NotifyService } from 'app/services/notify.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-show-profile',
  templateUrl: './show-profile.component.html',
  styleUrls: ['./show-profile.component.css']
})
export class ShowProfileComponent implements OnInit {
  Data:any;
  tab:number=1;
  orderTotal:number;
  myURL:any;
  showpage=true;
  constructor( private kitchenService:KitchenService,
    private notifyService: NotifyService,
    private languageService:LanguageService,
    private activeRoute: ActivatedRoute,) { }

  ngOnInit() {
  this.myURL=environment.api_img;
    this.kitchenService.GetByID(+this.activeRoute.snapshot.paramMap.get('id')).subscribe(
      res=>{
        this.orderTotal=0;
        this.showpage=false;this.Data=res.Data;
        this.Data.orders.forEach(element => {
          this.orderTotal+=element.NetTotal
        });
      })
  }

  approve(status){
    this.kitchenService.Approve({UserID:this.Data.chief.user_id,is_approved:status}).subscribe(res=>{
      if(res.Success){
       this.notifyService.success(res.Message);
      }
      else{
       this.notifyService.error(res.Message);
      }
     }) 
 }

}

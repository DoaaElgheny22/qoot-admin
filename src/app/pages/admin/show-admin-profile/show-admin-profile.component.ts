
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { RolesService } from 'app/services/admin/role.service';
import { CityService } from 'app/services/settings/cities.service';
import { LanguageService } from 'app/services/language.service';
import { NotifyService } from 'app/services/notify.service';
import { EmployeeService } from 'app/services/admin/Employee.service';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpRequest, HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';

@Component({
  selector: 'app-show-admin-profile',
  templateUrl: './show-admin-profile.component.html',
  styleUrls: ['./show-admin-profile.component.css']
})
export class ShowAdminProfileComponent implements OnInit {
  form: FormGroup;
  roles:any;
  fileData=null;
  img:any;
  showpage:boolean=false;
  cities:any;
  Data:any;
  myRoles:any=[];
  constructor(private formBuilder: FormBuilder,
    private rolesService:RolesService,
    private employeeService:EmployeeService,
    private cityService:CityService,
    public modalService:BsModalService,
    private activeRoute: ActivatedRoute,
    private languageService:LanguageService,
    private localStorageService:LocalStorageService,
    private notifyService: NotifyService,
    private http: HttpClient,
    ) { }

  ngOnInit() {
    this.showpage=false
   this.rolesService.GetList().subscribe(res=>{this.roles=res.Data})
   this.cityService.Getall().subscribe(res=>{this.cities=res.Data.data})
    this.initForm();
      this.employeeService.GetByID(+this.activeRoute.snapshot.paramMap.get('id')).subscribe(
        res=>{
          res.Data.address=res.Data.employee.address
          res.Data.firstname=res.Data.employee.firstname
          res.Data.lastname=res.Data.employee.lastname
          res.Data.city_id=res.Data.employee.city_id
          res.Data.country=res.Data.employee.country
          res.Data.postal_code=res.Data.employee.postal_code
          res.Data.role_id=res.Data.employee.roles[0].id;
          res.Data.UserID=res.Data.id;
          if(res.Data.avatar!=null){
            this.img=res.Data.avatar
          }
         this.form.patchValue(res.Data);
         this.showpage=true
      })
  }

  onSelectFile(event,type) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.fileData = <File>event.target.files[0];
      this.img=this.fileData
      reader.onload =(event:any) => {
          this.img=(event.target.result);
          this.uploadmyImage(this.fileData);
      }
}}
uploadmyImage(Data){
  const formData = new FormData();
  formData.append('avatar', Data);
  formData.append('UserID', this.activeRoute.snapshot.paramMap.get('id'));
  this.http.request(new HttpRequest('POST', `${environment.api_url_main}/User/UploadImage`, 
  formData, {reportProgress: true})).subscribe(
    event=>{
      if(event.type==3){this.notifyService.success("Image uploaded Sucessfully");
    }
  });
}

changePasswort(){
  this.modalService.show(ResetPasswordComponent,{ initialState: 
    { id:+this.activeRoute.snapshot.paramMap.get('id')}, class: 'modal-md' })
}
  initForm() {
    this.form = this.formBuilder.group({
      id: [0],
      UserID: [0],
      type:'admin',
      avatar:[null],
      email:[null, Validators.required],
      username: [null, Validators.required],
      phone:[null, [Validators.required,Validators.pattern('(05)[0-9]{8}')]],
      firstname: [null],
      lastname: [null, Validators.required],
      address:[null, Validators.required],
      country :[null, Validators.required],
      city_id:[null, Validators.required],
      postal_code:[null, Validators.required],
      roles:[],
      role_id:[null,Validators.required]
    });
  }
  save() {
    if (this.form.valid) {
      this.myRoles.push(this.form.value.role_id)
      this.form.value.roles=this.myRoles
      if (this.form.value.id == 0) {
        this.employeeService.Post(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
            } else {
              this.notifyService.error(res.Message[0]);
            }
          }
        );
      } else if (this.form.value.id > 0 && this.form.dirty) {
        this.employeeService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
            } else {
              this.notifyService.error(res.Message[0]);
            }
          }
        );
      }
    }
    else {
      for (let control in this.form.controls) {
        this.form.get(control).markAsDirty();
      }
    }
  }
}

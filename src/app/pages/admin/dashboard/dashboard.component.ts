import { Component, OnInit } from '@angular/core';
import { DashBoardService } from 'app/services/admin/dashboard.service';
import { LanguageService } from 'app/services/language.service';
import * as moment from 'moment-timezone';
import { DishesService } from 'app/services/admin/dishes.service';
import { NotifyService } from 'app/services/notify.service';
import { CustomersService } from 'app/services/admin/customers.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  TopOrderChiefs:any=[];
  myparam:any;
  EndDate:any=null;
  StartDate:any=null;
  loading:boolean=true;
  OrderData:any;
  TopRateChiefs:any=[];
  ChiefData:any;
  Income:any=[];
  PendingChief:any;
  NewCustomer:any;
  NewDish:any;
  constructor(private dashBoardService:DashBoardService,
    private dishesService:DishesService,
    private customersService:CustomersService,
    private notifyService:NotifyService,
    public languageService:LanguageService) { }

  ngOnInit() {
    this.StartDate=new Date();
    this.EndDate=new Date();
    this.DisplayData();

  }
  approve(ID){
    this.dishesService.Approve(ID).subscribe(res=>{this.notifyService.success(res.Message);}) 
 }
 Block(ID){
  this.customersService.Block(ID).subscribe(res=>{
    if(res.Success){
      this.notifyService.success(res.Message);
    }
    else{
      this.notifyService.error(res.Message);
    }
    }) 
}
  DisplayData(){
    this.loading=true;
    this.myparam={StartDate:this.StartDate==null?null:moment(this.StartDate).format('YYYY-MM-DD'),
    EndDate:this.StartDate==null?null:moment(this.EndDate).format('YYYY-MM-DD'),
    orderBy:'id',
    page:1}
    this.dashBoardService.ChiefData(this.myparam).subscribe(res=>{this.ChiefData=res.Data;

    this.dashBoardService.OrderData(this.myparam).subscribe(res=>{this.OrderData=res.Data;this.loading=false
  
    })
    })

    this.dashBoardService.TopOrderChiefs().subscribe(res=>{this.TopOrderChiefs=res.Data})
    this.dashBoardService.TopRateChiefs().subscribe(res=>{this.TopRateChiefs=res.Data})
    this.dashBoardService.Income(null).subscribe(res=>{this.Income=res.Data})
    this.dashBoardService.PendingChief(this.myparam).subscribe(res=>{this.PendingChief=res.Data})
    this.dashBoardService.NewCustomer(this.myparam).subscribe(res=>{this.NewCustomer=res.Data})
    this.dashBoardService.NewDish(this.myparam).subscribe(res=>{this.NewDish=res.Data;})

  }

}

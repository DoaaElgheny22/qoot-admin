import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffMangmentComponent } from './staff-mangment.component';

describe('StaffMangmentComponent', () => {
  let component: StaffMangmentComponent;
  let fixture: ComponentFixture<StaffMangmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffMangmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffMangmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

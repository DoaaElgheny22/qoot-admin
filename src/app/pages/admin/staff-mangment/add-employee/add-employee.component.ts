import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { RolesService } from 'app/services/admin/role.service';
import { CityService } from 'app/services/settings/cities.service';
import { LanguageService } from 'app/services/language.service';
import { NotifyService } from 'app/services/notify.service';
import { EmployeeService } from 'app/services/admin/Employee.service';
import { ResetPasswordComponent } from '../../reset-password/reset-password.component';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  form: FormGroup;
  roles:any;
  cities:any;
  onClose: any;
  id:number;
  Data:any;
  myRoles:any=[];
  constructor(private formBuilder: FormBuilder,
    private rolesService:RolesService,
    private employeeService:EmployeeService,
    public modalService:BsModalService,
    private cityService:CityService,
    private languageService:LanguageService,
    private notifyService: NotifyService,
    public myModel: BsModalRef,) { }

  ngOnInit() {
   this.rolesService.GetList().subscribe(res=>{this.roles=res.Data})
   this.cityService.Getall().subscribe(res=>{this.cities=res.Data.data})
    this.initForm();
    if(this.id>0){
   
      this.employeeService.GetByID(this.id).subscribe(res=>{console.log(res.Data,"edrftgh")})
      this.Data.address=this.Data.employee.address
      this.Data.firstname=this.Data.employee.firstname
      this.Data.lastname=this.Data.employee.lastname
      this.Data.city_id=this.Data.employee.city_id
      this.Data.country=this.Data.employee.country
      this.Data.postal_code=this.Data.employee.postal_code
      this.Data.role_id=this.Data.employee.roles[0].id,
      this.Data.UserID=this.id,
      
      this.form.patchValue(this.Data);
      this.form.get('password').setValidators(null);
      this.form.get('password').updateValueAndValidity();
  
    }
    else{
      this.form.get('password').setValidators(Validators.required);
      this.form.get('password').updateValueAndValidity();
    }
  }
  initForm() {
    this.form = this.formBuilder.group({
      id: [0],
      UserID: [0],
      type:'admin',
      email:[null, Validators.required],
      username: [null, Validators.required],
      phone:[null, [Validators.required,Validators.pattern('(05)[0-9]{8}')]],
      password: [null],
      firstname: [null],
      lastname: [null, Validators.required],
      address:[null, Validators.required],
      country :[null, Validators.required],
      city_id:[null, Validators.required],
      postal_code:[null, Validators.required],
      roles:[],
      role_id:[null,Validators.required]
    });
  }
  changePasswort(){
    this.modalService.show(ResetPasswordComponent,{ initialState: 
      { id:+this.form.value.UserID}, class: 'modal-md' })
  }
  save() {
    if (this.form.valid) {
      this.myRoles.push(this.form.value.role_id)
      this.form.value.roles=this.myRoles
      if (this.form.value.id == 0) {
        this.employeeService.Post(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              console.log(res.Message[0],"kkkk")
              this.notifyService.error(res.Message[0]);
            }
          }
        );
      } else if (this.form.value.id > 0 && this.form.dirty) {
        this.employeeService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              console.log(res.Message[0],"kkkk")
              this.notifyService.error(res.Message[0]);
            }
          }
        );
      }
    }
    else {
      for (let control in this.form.controls) {
        this.form.get(control).markAsDirty();
      }
    }
  }
}

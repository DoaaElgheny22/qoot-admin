
import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { LanguageService } from 'app/services/language.service';
import { KitchenService } from 'app/services/admin/kitchen.service';
import { DataTableDirective } from 'angular-datatables';
import { environment } from 'environments/environment';
import { NotifyService } from 'app/services/notify.service';
import { DishesService } from 'app/services/admin/dishes.service';

@Component({
  selector: 'app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.css']
})
export class DishesComponent implements OnInit {
 Data:any={};
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'ID',
    isAscending: true 
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private dishesService:DishesService,
  private notifyService: NotifyService,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
  this.showpage=true
   this.getData();
   this.dishesService.GetListData().subscribe(res=>{this.totalData=res.Data;
    this.showpage=false
    })
  }

  getData(){
   this.dishesService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
       this.Data = res.Data;
      }
    })
  }
  view(status){
    this.pagingparamater.sortBy=status
    this.getData();
  }
  approve(ID){
     this.dishesService.Approve(ID).subscribe(res=>{this.notifyService.success(res.Message);}) 
  }
 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }
 Export() {
  this.dishesService.Export().subscribe(res => {
    let path = res.Data;
    window.open(`${environment.api_url_rep}${path}`);
  });
}
}


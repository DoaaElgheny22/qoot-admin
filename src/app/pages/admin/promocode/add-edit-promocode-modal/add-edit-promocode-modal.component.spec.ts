import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPromocodeModalComponent } from './add-edit-promocode-modal.component';

describe('AddEditPromocodeModalComponent', () => {
  let component: AddEditPromocodeModalComponent;
  let fixture: ComponentFixture<AddEditPromocodeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditPromocodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPromocodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { RolesService } from 'app/services/admin/role.service';
import { CityService } from 'app/services/settings/cities.service';
import { LanguageService } from 'app/services/language.service';
import { NotifyService } from 'app/services/notify.service';
import { EmployeeService } from 'app/services/admin/Employee.service';
import { PromotionService } from 'app/services/admin/promocode.service';

@Component({
  selector: 'app-add-edit-promocode-modal',
  templateUrl: './add-edit-promocode-modal.component.html',
  styleUrls: ['./add-edit-promocode-modal.component.css']
})
export class AddEditPromocodeModalComponent implements OnInit {
  form: FormGroup;
  roles:any;
  cities:any;
  onClose: any;
  id:number;
  Data:any;
  myRoles:any=[];
  constructor(private formBuilder: FormBuilder,
    private rolesService:RolesService,
    private promotionService:PromotionService,
    private cityService:CityService,
    private languageService:LanguageService,
    private notifyService: NotifyService,
    public myModel: BsModalRef,) { }

  ngOnInit() {
   this.rolesService.GetList().subscribe(res=>{this.roles=res.Data})
   this.cityService.Getall().subscribe(res=>{this.cities=res.Data.data})
    this.initForm();
    if(this.id>0){
      console.log("ll",this.Data.dis_count_type)
      this.form.patchValue(this.Data);
      if(this.Data.dis_count_type==1){
        this.form.get('dis_count_type').setValue(1);
      }else{
        this.form.get('dis_count_type').setValue(2);
      }
    
    }
    else{
      this.form.get('dis_count_type').setValue(2);
    }
  }
  initForm() {
    this.form = this.formBuilder.group({
      id: [0],
      name: [null, Validators.required],
      recommendation_from:[null, Validators.required],
      description: [null],
      dis_count_value:[null, Validators.required],
      dis_count_type:[null, Validators.required],
      promo_code:[null, Validators.required],
      promo_count:[null, Validators.required],
      start_date:[null,Validators.required],
      end_date:[null,Validators.required]
    });
  }
  generate(){
   let code= Math.floor(10000 + Math.random() * 90000);
   this.checkpromo(code);
   
  }
  checkpromo(code){
    this.promotionService.checkPromoCode(code).subscribe(
      res=>{
        if(res.Success){
          this.form.get('promo_code').setValue(code);
        }
        else{
          this.generate();
        }
      }
    )   
  }
  type(type){
    this.form.get('dis_count_type').setValue(type);
  }
  save() {
    if (this.form.valid) {

      if (this.form.value.id == 0) {
        this.promotionService.Post(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              this.notifyService.error(res.Message[0]);
            }
          }
        );
      } else if (this.form.value.id > 0 && this.form.dirty) {
        this.promotionService.Update(this.form.value).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.myModel.hide();
              this.onClose();
            } else {
              this.notifyService.error(res.Message[0]);
            }
          }
        );
      }
   }
    else {
      for (let control in this.form.controls) {
        this.form.get(control).markAsDirty();
      }
    }
  }
}

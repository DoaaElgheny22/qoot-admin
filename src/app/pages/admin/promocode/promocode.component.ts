

import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { LanguageService } from 'app/services/language.service';
import { KitchenService } from 'app/services/admin/kitchen.service';
import { DataTableDirective } from 'angular-datatables';
import { environment } from 'environments/environment';
import { NotifyService } from 'app/services/notify.service';
import { DishesService } from 'app/services/admin/dishes.service';
import { EmployeeService } from 'app/services/admin/Employee.service';
import { ConfirmationBoxService } from 'app/services/confirmation-box.service';
import { AddEditPromocodeModalComponent } from './add-edit-promocode-modal/add-edit-promocode-modal.component';
import { PromotionService } from 'app/services/admin/promocode.service';
@Component({
  selector: 'app-promocode',
  templateUrl: './promocode.component.html',
  styleUrls: ['./promocode.component.css']
})
export class PromocodeComponent implements OnInit {
 Data:any={};
 addEditModel:BsModalRef;
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'ID',
    isAscending: true 
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private promotionService:PromotionService,
  private confirmationBoxService: ConfirmationBoxService,
  private notifyService: NotifyService,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
  this.showpage=true
   this.getData();
   this.promotionService.GetList(this.pagingparamater).subscribe(res=>{this.totalData=res.Data;
    this.showpage=false
    })
  }

  addpromo(ID=0,Data=null){
    this.addEditModel = this.modalService.show(AddEditPromocodeModalComponent,{ initialState: 
      { id:ID,Data:Data}, class: 'modal-lg' })
      this.addEditModel.content.onClose  = (res)=>{
      this.getData();    
      }
  }
 
  getData(){
   this.promotionService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
      this.Data = res.Data;
      }
    })
  }

  Delete(id) {
    this.confirmationBoxService.confirm({ ar: 'برجاء تاكيد حذف ؟', en: 'Confirm Delete??' }, isConfirmed => {
      if (isConfirmed) {
        this.promotionService.Delete(id).subscribe(
          res => {
            if (res.Success) {
              this.notifyService.success(res.Message);
              this.getData();
            } else {
              this.notifyService.error(res.Message);
            }
          }
        );
      }
    });
   }
 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }

}

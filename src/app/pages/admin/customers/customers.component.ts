import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { LanguageService } from 'app/services/language.service';
import { KitchenService } from 'app/services/admin/kitchen.service';
import { DataTableDirective } from 'angular-datatables';
import { environment } from 'environments/environment';
import { NotifyService } from 'app/services/notify.service';
import { DishesService } from 'app/services/admin/dishes.service';
import { CustomersService } from 'app/services/admin/customers.service';
import { ShowCustomerComponent } from './show-customer/show-customer.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
 Data:any={};
  pagingparamater = 
  {current_page: 1,
    name:'',
    per_page: 10,
    sortBy:'all',
    orderBy: 'ID',
    isAscending: true 
  };
  dtOptions:any;
  loading:boolean=false;
  tabactive:string;
  showpage:boolean=false;
  dtElement: DataTableDirective;
  totalData:any;
  constructor(
  private customersService: CustomersService,
  private notifyService: NotifyService,
  private activatedRoute: ActivatedRoute,
  private modalService: BsModalService,
  private languageService:LanguageService) { }

  ngOnInit() {
    this.tabactive=this.activatedRoute.routeConfig.path
    this.showpage=true
    this.getData();
    this.customersService.GetListData().subscribe(res=>{this.totalData=res.Data;
    this.showpage=false
    })
  }
  Export() {
    this.customersService.Export().subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }
  Block(ID){
    this.customersService.Block(ID).subscribe(res=>{
      if(res.Success){
        this.notifyService.success(res.Message);
        this.getData();
      }
      else{
        this.notifyService.error(res.Message);
      }
      }) 
 }
  getData(){
   this.customersService.GetList(this.pagingparamater).subscribe(
    res => {
      if (res.Success) {
       this.Data = res.Data;
      }
    })
  }
  view(status){
    this.pagingparamater.sortBy=status
    this.getData();
  }


 pageChanged(event: any): void {
  this.pagingparamater.current_page = event.page;
  !this.loading && this.getData();
 }

}



import { Component, OnInit } from '@angular/core';
import { CustomersService } from 'app/services/admin/customers.service';
import { ActivatedRoute } from '@angular/router';
import { NotifyService } from 'app/services/notify.service';
import { LanguageService } from 'app/services/language.service';

@Component({
  selector: 'app-show-customer',
  templateUrl: './show-customer.component.html',
  styleUrls: ['./show-customer.component.css']
})
export class ShowCustomerComponent implements OnInit {
Data:any;
tab:number=1;
showpage=true;
  constructor( private customersService:CustomersService,
               private activeRoute: ActivatedRoute,
               private notifyService: NotifyService,
               public languageService:LanguageService,
              ) { }

  ngOnInit() {
    this.showpage=true
    this.customersService.GetByID(+this.activeRoute.snapshot.paramMap.get('id')).subscribe(
      res=>{this.showpage=false;this.Data=res.Data})
  }

  block(event: any){
    this.customersService.Block(+this.activeRoute.snapshot.paramMap.get('id')).subscribe(
      res=>{
        this.showpage=true
        this.customersService.GetByID(+this.activeRoute.snapshot.paramMap.get('id')).subscribe(
          res=>{this.showpage=false;this.Data=res.Data})
        this.notifyService.success(res.Message);
      }
    )
  }
}

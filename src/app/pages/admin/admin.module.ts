

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TreeModule } from 'ng2-tree';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from 'app/sharedModules/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { ModalModule } from 'ngx-bootstrap';
import { AdminRoutingModule } from './admin-routing.module';
import { KitchensComponent } from './kitchens/kitchens.component';
import { DishesComponent } from './dishes/dishes.component';
import { OrdersComponent } from './orders/orders.component';
import { CustomersComponent } from './customers/customers.component';
import { StaffMangmentComponent } from './staff-mangment/staff-mangment.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddEmployeeComponent } from './staff-mangment/add-employee/add-employee.component';
import { EmployeeService } from 'app/services/admin/Employee.service';
import { KitchenService } from 'app/services/admin/kitchen.service';
import { ShowProfileComponent } from './kitchens/show-profile/show-profile.component';
import { DishesService } from 'app/services/admin/dishes.service';
import { CustomersService } from 'app/services/admin/customers.service';
import { ShowCustomerComponent } from './customers/show-customer/show-customer.component';
import { RolesComponent } from './roles/roles.component';
import { AddRoleComponent } from './roles/add-role/add-role.component';
import { RolesService } from 'app/services/admin/role.service';
import { OrderService } from 'app/services/admin/order.service';
import { OrderDetailComponent } from './orders/order-detail/order-detail.component';
import { CityService } from 'app/services/settings/cities.service';
import { DashBoardService } from 'app/services/admin/dashboard.service';
import { PromocodeComponent } from './promocode/promocode.component';
import { AddEditPromocodeModalComponent } from './promocode/add-edit-promocode-modal/add-edit-promocode-modal.component';
import { PromotionService } from 'app/services/admin/promocode.service';
import { ShowAdminProfileComponent } from './show-admin-profile/show-admin-profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { InvoiceReciptComponent } from './orders/invoice-recipt/invoice-recipt.component';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/admin/', '.json');
}
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
    ModalModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    DataTablesModule.forRoot(),
    TreeModule
  ],
  declarations: [
    KitchensComponent,
    DishesComponent,
    OrdersComponent,
    CustomersComponent,
    StaffMangmentComponent,
    DashboardComponent,
    AddEmployeeComponent,
    ShowProfileComponent,
    ShowCustomerComponent,
    RolesComponent,
    AddRoleComponent,
    OrderDetailComponent,
    PromocodeComponent,
    AddEditPromocodeModalComponent,
    InvoiceReciptComponent,
    ShowAdminProfileComponent,ResetPasswordComponent
  ],
  providers: [
    EmployeeService,
    KitchenService,
    DishesService,
    CustomersService,
    RolesService,
    OrderService,
    CityService,
    DashBoardService,
    PromotionService,

  ],
  entryComponents: [
    AddEmployeeComponent,
    OrderDetailComponent,
    AddEditPromocodeModalComponent,
    ResetPasswordComponent
  ]
})
export class AdminModule { }


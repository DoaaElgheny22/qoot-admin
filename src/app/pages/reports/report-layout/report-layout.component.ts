import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { LanguageService } from 'app/services/language.service';
@Component({
  selector: 'app-report-layout',
  templateUrl: './report-layout.component.html',
  styleUrls: ['./report-layout.component.css']
})
export class ReportLayoutComponent implements OnInit {
  tabactive:string;
settingsPermission:any=[];
Data:any=[];
@Input("mycurrenttab") mycurrenttab: any = null;
  constructor(private activeRoute: ActivatedRoute,
    public languageService: LanguageService,
               public localStorageService:LocalStorageService) { }

  ngOnInit() {
    this.tabactive=this.mycurrenttab
    this.Data=this.localStorageService.get('allpermissions');
    console.log(this.Data);
    
    this.settingsPermission= this.Data.filter(i=>i.parent_id==5 && i.type=='view');
  }

}

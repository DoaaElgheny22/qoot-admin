import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishReportComponent } from './dish-report.component';

describe('DishReportComponent', () => {
  let component: DishReportComponent;
  let fixture: ComponentFixture<DishReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DishReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

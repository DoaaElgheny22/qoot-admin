
import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../services/reports/reports.service';
import { environment } from 'environments/environment';
import * as moment from 'moment-timezone';
import { LanguageService } from 'app/services/language.service';
import { DashBoardService } from 'app/services/admin/dashboard.service';
@Component({
  selector: 'app-dish-report',
  templateUrl: './dish-report.component.html',
  styleUrls: ['./dish-report.component.css']
})
export class DishReportComponent implements OnInit {
  Data:any={};
  allCity:any;
  allCategory:any;
  allchief:any;
  EndDate:any=null;
  StartDate:any=null;
  pagingparamater = 
  {
  current_page: 1,
  approved:2,
  per_page: 10,
  city_id:0,
  chief_id:0,
  category_id:0,
  StartDate:null,
  EndDate:null,
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  totalData:any;
  constructor(private reportsService:ReportsService,
               private dashBoardService:DashBoardService,
               private languageService:LanguageService) { }

  ngOnInit() {
    this.showpage=true;
     this.reportsService.GetallCity().subscribe(res=>{this.allCity=res.Data})
     this.reportsService.GetallCategory().subscribe(res=>{this.allCategory=res.Data});
     this.dashBoardService.AllChief().subscribe(res=>{ res.Data.map(i=>i.ar_name=i.kitchen_name);this.allchief=res.Data})
     this.getData();
     this.reportsService.GetListDataDish().subscribe(res=>{this.totalData=res.Data;
      this.showpage=false
      })
    }
  
  getData(){
    this.pagingparamater.StartDate=this.StartDate==null?null:moment(this.StartDate).format('YYYY-MM-DD'),
    this.pagingparamater.EndDate==null?null:moment(this.EndDate).format('YYYY-MM-DD');
    this.reportsService.GetListDish(this.pagingparamater).subscribe(
      res => {
        if (res.Success) {
         this.Data = res.Data;
        }
      })
    }

   pageChanged(event: any): void {
    this.pagingparamater.current_page = event.page;
    !this.loading && this.getData();
   }
   status(status){
     this.pagingparamater.approved=status
   }
   Export() {
    this.reportsService.ExportDish(this.pagingparamater).subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }

}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from 'app/sharedModules/layouts/layout.component';
import { RouterModule, Routes } from '@angular/router';
import { KitchenReportComponent } from './kitchen-report/kitchen-report.component';
import { CategoryReportComponent } from './category-report/category-report.component';
import { DishReportComponent } from './dish-report/dish-report.component';
import { CustomerReportComponent } from './customer-report/customer-report.component';
import { OrderReportComponent } from './order-report/order-report.component';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { QootIncomeComponent } from './qoot-income/qoot-income.component';
import { PaymentStatusComponent } from './payment-status/payment-status.component';
import { VisitorReportComponent } from './visitor-report/visitor-report.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'kitchen-report', component: KitchenReportComponent },
      { path: 'category-report', component: CategoryReportComponent },
      { path: 'dish-report', component: DishReportComponent },
      { path: 'customer-report', component: CustomerReportComponent },
      { path: 'order-report', component: OrderReportComponent },
      {path:'transaction_history',component:TransactionHistoryComponent},
      {path:'qoot-income',component:QootIncomeComponent},
      {path:'payment-status',component:PaymentStatusComponent},
      {path:'visitors_report',component:VisitorReportComponent},
    ]
  }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }





import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../services/reports/reports.service';
import { environment } from 'environments/environment';
import * as moment from 'moment-timezone';
import { LanguageService } from 'app/services/language.service';
import { DashBoardService } from 'app/services/admin/dashboard.service';
import { NotifyService } from 'app/services/notify.service';
import { HttpRequest, HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
@Component({
  selector: 'app-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.css']
})
export class PaymentStatusComponent implements OnInit {
  Data:any={};
  allchief:any;
  file:any;
  filepath:null;
  fileData=null;
  pagingparamater = 
  {
  current_page: 1,
  per_page: 10,
  chief_id:0,
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  totalData:any;
  constructor(private reportsService:ReportsService,
               private notifyService: NotifyService,
               private http: HttpClient,
               private dashBoardService:DashBoardService,
               private languageService:LanguageService) { }

  ngOnInit(){
    this.dashBoardService.AllChief().subscribe(res=>{ res.Data.map(i=>i.ar_name=i.kitchen_name);this.allchief=res.Data})
    this.showpage=true;
     this.getData();
    }
  getData(){
    this.reportsService.GetListPaymentStatus(this.pagingparamater).subscribe(
      res => {
        if (res.Success) {
          this.showpage=false;
         this.Data = res.Data;
        }
      })
    }
   pageChanged(event: any): void {
    this.pagingparamater.current_page = event.page;
    !this.loading && this.getData();
   }
   Export() {
    this.reportsService.ExportPaymentStatus(this.pagingparamater).subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.fileData = <File>event.target.files[0];
      this.file=this.fileData
      reader.onload =(event:any) => {
          this.file=(event.target.result);
          this.uploadmyImage(this.fileData);
      }
}}
uploadmyImage(Data){
  const formData = new FormData();
  formData.append('file',Data);
  this.http.request(new HttpRequest('POST',`${environment.api_url_main}/UploadFile`,
  formData,{ reportProgress: true })).subscribe(
    event => {
      if (event.type === HttpEventType.Response) {
       if (event.body['Success']) {
       this.filepath=event.body['Data'].file;
        } else {
          this.notifyService.error("something wrong upload again");
        }
      }
    },
  );
}
DownloadFile(){
  this.reportsService.DownloadFile().subscribe(res => {
    let path = res.Data;
    window.open(`${environment.api_url_rep}${path}`);
  });
}
Import(){
    this.reportsService.Import({file:this.filepath}).subscribe(res => {
      if(res.Success){
          this.notifyService.success(res.Message);
          this.getData();
      }
      else{
        this.notifyService.success(res.Message)
      }
    });
}

}

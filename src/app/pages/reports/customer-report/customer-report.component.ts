
import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../services/reports/reports.service';
import { environment } from 'environments/environment';
import * as moment from 'moment-timezone';
import { LanguageService } from 'app/services/language.service';
import { DashBoardService } from 'app/services/admin/dashboard.service';
@Component({
  selector: 'app-customer-report',
  templateUrl: './customer-report.component.html',
  styleUrls: ['./customer-report.component.css']
})
export class CustomerReportComponent implements OnInit {
  Data:any={};
  allCity:any;
  allCategory:any;
  EndDate:any=null;
  StartDate:any=null;
  allchief:any;
  pagingparamater = 
  {
  current_page: 1,
  per_page: 10,
  StartDate:null,
  EndDate:null,
  status:'all'
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  totalData:any;
  constructor(private reportsService:ReportsService,
               private dashBoardService:DashBoardService,
               private languageService:LanguageService) { }

  ngOnInit() {
    this.showpage=true;
     this.getData();
     this.reportsService.GetListDataCustomer().subscribe(res=>{this.totalData=res.Data;
      this.showpage=false
      })
    }
  
  getData(){
    this.pagingparamater.StartDate=this.StartDate==null?null:moment(this.StartDate).format('YYYY-MM-DD'),
    this.pagingparamater.EndDate==null?null:moment(this.EndDate).format('YYYY-MM-DD');
    this.reportsService.GetListCustomer(this.pagingparamater).subscribe(
      res => {
        if (res.Success) {
        this.Data = res.Data;
        }
      })
    }
    status(status){
      this.pagingparamater.status=status
    }
   pageChanged(event: any): void {
    this.pagingparamater.current_page = event.page;
    !this.loading && this.getData();
   }
   Export() {
    this.reportsService.ExportCustomer(this.pagingparamater).subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }

}

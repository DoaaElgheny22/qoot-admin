
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from 'app/sharedModules/shared.module';
import { ModalModule } from 'ngx-bootstrap';
import { ReportsRoutingModule } from './reports-routing.module';
import { KitchenReportComponent } from './kitchen-report/kitchen-report.component';
import { ReportLayoutComponent } from './report-layout/report-layout.component';
import { CategoryReportComponent } from './category-report/category-report.component';
import { ReportsService } from '../../services/reports/reports.service';
import { DashBoardService } from 'app/services/admin/dashboard.service';
import { DishReportComponent } from './dish-report/dish-report.component';
import { CustomerReportComponent } from './customer-report/customer-report.component';
import { OrderReportComponent } from './order-report/order-report.component';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { QootIncomeComponent } from './qoot-income/qoot-income.component';
import { PaymentStatusComponent } from './payment-status/payment-status.component';
import { VisitorReportComponent } from './visitor-report/visitor-report.component';
const routes: Routes = [];
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/reports/', '.json');
}
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReportsRoutingModule,
    ModalModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  
  ],
  declarations: [
    KitchenReportComponent,
    ReportLayoutComponent,
    CategoryReportComponent,
    DishReportComponent,
    CustomerReportComponent,
    OrderReportComponent,
    TransactionHistoryComponent,
    QootIncomeComponent,
    PaymentStatusComponent,
    VisitorReportComponent,
  ],
  providers: [
    ReportsService,
    DashBoardService
  ],
  entryComponents: [
  
  ]
})
export class ReportsModule { }


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QootIncomeComponent } from './qoot-income.component';

describe('QootIncomeComponent', () => {
  let component: QootIncomeComponent;
  let fixture: ComponentFixture<QootIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QootIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QootIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

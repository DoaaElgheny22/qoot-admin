

import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../services/reports/reports.service';
import { environment } from 'environments/environment';
import * as moment from 'moment-timezone';
import { LanguageService } from 'app/services/language.service';
import { DashBoardService } from 'app/services/admin/dashboard.service';
import { HttpRequest, HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { NotifyService } from 'app/services/notify.service';
@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.css']
})
export class TransactionHistoryComponent implements OnInit {
  Data:any={};
  file:any;
  filepath:any;
  allchief:any;
  fileData=null;
  EndDate:any=null;
  StartDate:any=null;
  pagingparamater = 
  {
  current_page: 1,
  status:'',
  per_page: 10,
  chief_id:0,
  StartDate:null,
  EndDate:null,
  payment_method_id:0,
  customer_id:0
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  totalData:any;
  constructor(private reportsService:ReportsService,
               private notifyService: NotifyService,
               private http: HttpClient,
               private dashBoardService:DashBoardService,
               private languageService:LanguageService) { }

  ngOnInit() {
    this.showpage=true;
    this.dashBoardService.AllChief().subscribe(res=>{ res.Data.map(i=>i.ar_name=i.kitchen_name);this.allchief=res.Data})
     this.getData();
    }
  
  getData(){
    this.pagingparamater.StartDate=this.StartDate==null?null:moment(this.StartDate).format('YYYY-MM-DD'),
    this.pagingparamater.EndDate==null?null:moment(this.EndDate).format('YYYY-MM-DD');
    this.reportsService.GetListTransactionHistory(this.pagingparamater).subscribe(
      res => {
        if (res.Success) {
         this.Data = res.Data;
        }
      })
    }

   pageChanged(event: any): void {
    this.pagingparamater.current_page = event.page;
    !this.loading && this.getData();
   }
   status(status){
     this.pagingparamater.status=status
   }

   Export() {
    this.reportsService.ExportTransactionHistory(this.pagingparamater).subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }
 
}


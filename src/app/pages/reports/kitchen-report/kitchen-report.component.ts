
import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../services/reports/reports.service';
import { environment } from 'environments/environment';
import * as moment from 'moment-timezone';
import { LanguageService } from 'app/services/language.service';
import { DashBoardService } from 'app/services/admin/dashboard.service';

@Component({
  selector: 'app-kitchen-report',
  templateUrl: './kitchen-report.component.html',
  styleUrls: ['./kitchen-report.component.css']
})
export class KitchenReportComponent implements OnInit {
  Data:any={};
  allCity:any;
  EndDate:any=null;
  StartDate:any=null;
  pagingparamater = 
  {
  current_page: 1,
  status:'',
  per_page: 10,
  city_id:0,
  StartDate:null,
  EndDate:null,
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  totalData:any;
  constructor(private reportsService:ReportsService,
               private dashBoardService:DashBoardService,
               private languageService:LanguageService) { }

  ngOnInit() {
    this.showpage=true;
     this.reportsService.GetallCity().subscribe(res=>{this.allCity=res.Data})
     this.reportsService.GetallCity().subscribe(res=>{this.allCity=res.Data})
     
     this.getData();
     this.reportsService.GetListDataChief().subscribe(res=>{this.totalData=res.Data;
      this.showpage=false
      })
    }
  
  getData(){
    this.pagingparamater.StartDate=this.StartDate==null?null:moment(this.StartDate).format('YYYY-MM-DD'),
    this.pagingparamater.EndDate==null?null:moment(this.EndDate).format('YYYY-MM-DD');
    this.reportsService.GetListChief(this.pagingparamater).subscribe(
      res => {
        if (res.Success) {
         this.Data = res.Data;
        }
      })
    }

   pageChanged(event: any): void {
    this.pagingparamater.current_page = event.page;
    !this.loading && this.getData();
   }
   status(status){
     this.pagingparamater.status=status
   }
   Export() {
    this.reportsService.ExportChief(this.pagingparamater).subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }

}


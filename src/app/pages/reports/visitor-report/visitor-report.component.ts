
import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../services/reports/reports.service';
import { environment } from 'environments/environment';
import * as moment from 'moment-timezone';
import { LanguageService } from 'app/services/language.service';
import { DashBoardService } from 'app/services/admin/dashboard.service';
import { HttpRequest, HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { NotifyService } from 'app/services/notify.service';
@Component({
  selector: 'app-visitor-report',
  templateUrl: './visitor-report.component.html',
  styleUrls: ['./visitor-report.component.css']
})
export class VisitorReportComponent implements OnInit {
  Data:any={};
  file:any;
  filepath:any;
  allchief:any;
  fileData=null;
  EndDate:any=null;
  StartDate:any=null;
  pagingparamater = 
  {
  current_page: 1,
  per_page: 10,
  chief_id:0,
  StartDate:null,
  EndDate:null,
  isAscending:true,
  orderBy:'chief_id',
  };
  dtOptions:any;
  loading:boolean=false;
  showpage:boolean=false;
  totalData:any;
  constructor(private reportsService:ReportsService,
               private notifyService: NotifyService,
               private http: HttpClient,
               private dashBoardService:DashBoardService,
               private languageService:LanguageService) { }

  ngOnInit() {
    this.showpage=true
    this.reportsService.VisitorData().subscribe(res=>{this.totalData=res.Data;
      this.showpage=false
      })
     this.getData();
    }
  
  getData(){
    this.pagingparamater.StartDate=this.StartDate==null?null:moment(this.StartDate).format('YYYY-MM-DD'),
    this.pagingparamater.EndDate==null?null:moment(this.EndDate).format('YYYY-MM-DD');
    this.reportsService.VisitorGetList(this.pagingparamater).subscribe(
      res => {
        if (res.Success) {
         this.Data = res.Data;
        }
      })
    }

   pageChanged(event: any): void {
    this.pagingparamater.current_page = event.page;
    !this.loading && this.getData();
   }
 

   Export() {
    this.reportsService.ExportVisitor(this.pagingparamater).subscribe(res => {
      let path = res.Data;
      window.open(`${environment.api_url_rep}${path}`);
    });
  }
 
}


import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { ModalModule, PaginationModule, PopoverModule, BsDatepickerModule, BsDropdownModule, BsLocaleService, BsDatepickerConfig } from 'ngx-bootstrap';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { enGbLocale, arLocale } from 'ngx-bootstrap/locale';
defineLocale('en', enGbLocale);
defineLocale('ar', arLocale);
import { FormatedDatePipe } from "./Formated-datedate.pipe";
import { environment } from "../../environments/environment";
import { LayoutModule } from "./layouts/layout.module";
import { ConfirmationBoxModule } from "./confirmation-box/confirmation-box.module";
import { SelectGetListModule } from "./select-get-list/select-get-list.module";
import { SelectGetMultiModule } from "./select-get-multi/select-get-multi.module";
import { FilesUploadModule } from "./files-upload/files-upload.module";
import { LanguageService } from '../services/language.service';
import { WebApiService } from '../services/webApi.service';
import { SubLayoutService } from '../services/sub-layout.service';
//import { AuthGuard } from '../services/user/guards/auth.guard';
import { MaskModule } from './mask/mask.module';
import { NgxBootstrapSwitchModule } from 'ngx-bootstrap-switch';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    LayoutModule,
    ConfirmationBoxModule,
    SelectGetListModule,
    FilesUploadModule,
    SelectGetMultiModule,
    MaskModule,
    NgxBootstrapSwitchModule.forRoot(),


  ],
  declarations: [
    FormatedDatePipe
  ],
  exports: [
    ModalModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    PopoverModule,
    BsDatepickerModule,
    BsDropdownModule,
    SelectGetListModule,
    FilesUploadModule,
    SelectGetMultiModule,
    FormatedDatePipe,
    MaskModule,
    NgxBootstrapSwitchModule,

 
  ],
  providers: [
    // {provide: ToastOptions, useClass: CustomOption},
    // AuthGuard,
    WebApiService,
    TranslateService,
    LanguageService,
    SubLayoutService,
  ]
})
export class SharedModule {
  constructor(
    private translateService: TranslateService,
    private languageService: LanguageService,
    private localeService: BsLocaleService,
    private bsDatepickerConfig: BsDatepickerConfig
  ) {
    this.translateService.use(this.languageService.getLanguageOrDefault());
    this.bsDatepickerConfig.dateInputFormat = environment.defaultDateFormat;
    this.bsDatepickerConfig.containerClass = 'theme-dark-blue';
  }
}

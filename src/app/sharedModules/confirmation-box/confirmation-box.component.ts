import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LanguageService } from "../../services/language.service";
@Component({
	selector: 'app-confirmation-box',
	templateUrl: 'confirmation-box.component.html',
	styleUrls: ['confirmation-box.component.css']
})
export class ConfirmationBoxComponent {
	confireBox: BsModalRef;
	confireBoxClose: any;
	arMsg: string;
	enMsg: string;
	constructor(public languageService : LanguageService) {}
   //private modalService: BsModalService
	confirm(): void {
	  this.confireBoxClose(true);
	}
   
	decline(): void {
	  this.confireBoxClose(false);
	}
}

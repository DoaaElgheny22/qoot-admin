import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { ConfirmationBoxService } from '../../services/confirmation-box.service';
import { ConfirmationBoxComponent } from './confirmation-box.component';


@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: null,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    ConfirmationBoxComponent
  ],
  exports: [ConfirmationBoxComponent],
  entryComponents:[ConfirmationBoxComponent],
  providers: [ConfirmationBoxService],
})
export class ConfirmationBoxModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/services/user/guards/auth.guard';
const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/pages/home/home.module#HomeModule',
  },
  {
    path: 'settings',
    loadChildren: 'app/pages/settings/settings.module#SettingsModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'admin',
    loadChildren: 'app/pages/admin/admin.module#AdminModule',
    canActivate: [AuthGuard],
  }
  ,
  {
    path: 'reports',
    loadChildren: 'app/pages/reports/reports.module#ReportsModule',
    canActivate: [AuthGuard],
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

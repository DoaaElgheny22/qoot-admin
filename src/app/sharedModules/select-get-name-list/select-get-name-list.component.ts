import { Component, Input, OnInit, forwardRef, ViewChild, ElementRef, Output, EventEmitter, OnChanges, SimpleChange, HostListener } from '@angular/core';
import { LanguageService } from "../../services/language.service";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-select-get-name-list',
	templateUrl: 'select-get-name-list.component.html',
	styleUrls: ['select-get-name-list.component.css'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => SelectGetNameListComponent),
			multi: true
		}
	]
})
export class SelectGetNameListComponent implements ControlValueAccessor, OnInit, OnChanges {
	@Input() dataService;
	@Input() dataSet;
	@Input() searchService;
	@Input() label;
	@Input() nolabel;
	@Input() isRequired: boolean = false;
	@Input() filterBy: any[] = null;
	@Input() searchServiceParam: string = 'name';
	@Input() disabledIDs: any[] = null;
	// @Input() groupBy: any = null;
	@Output() getFullObj: EventEmitter<any> = new EventEmitter<any>();
	@ViewChild('filterInput') private filterInput: ElementRef;
	// @ViewChild('mainPosition') mainPosition: ElementRef;
	// @ViewChild('mainEl') mainEl: ElementRef;
	isOpen: boolean = false;
	selectLoading: boolean = false;
	selectList: any[] = [];
	isDisabled: boolean = false;
	selected: any = {};
	selectedText: string;
	filteredList: any[] = [];
	writeValueID: number;
	sub: Subscription;
	constructor(public languageService: LanguageService) { }
	asd:Element
	ngOnInit() {
		this.selectLoading = true;
		if (this.dataService) {
			this.dataService.GetList().subscribe(res => {
				if (res.Success) {
					this.selectLoading = false;
					if (this.filterBy) {
						this.selectList = res.Data.filter(i => i[this.filterBy[0]] == this.filterBy[1]);
					} else {
						this.selectList = res.Data;
					}
					if (this.writeValueID) {
						this.selectItemFromList(this.selectList.find(i => i.ID == this.writeValueID));
					}
					// if (this.groupBy) {
					// 	this.selectList = this.groupByFN(this.selectList,i=>i.ItemCategory.ID)
					// console.log(
					// 		this.selectList
					// 	)
					// }
				}
			});
		} else if (this.dataSet && this.dataSet.length) {
			this.selectLoading = false;
			if (this.filterBy) {
				this.selectList = this.dataSet.filter(i => i[this.filterBy[0]] == this.filterBy[1]);
			} else {
				this.selectList = this.dataSet;
			}
		} else if (this.searchService) {
			this.searchServiceInput(name);
		}
	}
	searchServiceInput(name) {
		let filterObj = <any>{ pageSize: 10 }
		filterObj[this.searchServiceParam] = name;
		if (this.sub) this.sub.unsubscribe();
		this.sub = this.searchService.Search(filterObj).subscribe(res => {
			if (res.Success) {
				this.selectLoading = false;
				this.filteredList = this.selectList = res.Data.Result;
			}
		});
	}
	ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
		this.selectLoading = true;
		if(changes.disabledIDs){
			this.selectLoading = false;
		}
		if (changes.dataSet) {
			this.selectLoading = false;
			this.selectList = changes.dataSet.currentValue;
			if (this.writeValueID)
				this.selectItemFromList(this.selectList.find(i => i.ID == this.writeValueID));
		}
	}
	isInDisabledIDs(ID) :boolean{
		return this.disabledIDs ? this.disabledIDs.includes(ID) : false;
	}
	// groupByFN(array, f) {
	// 	var groups = {};
	// 	array.forEach(function (o) {
	// 		var group = JSON.stringify(f(o));
	// 		groups[group] = groups[group] || [];
	// 		groups[group].push(o);
	// 	});
	// 	return Object.keys(groups).map(function (group) {
	// 		return groups[group];
	// 	});
	// }
	filterChange(value) {
		if (value) {
			if (this.searchService) {
				this.searchServiceInput(value);
			} else {
				this.filteredList = this.selectList.filter(i => {
					let name = this.languageService.getLanguageOrDefault() == 'ar' ? i.Name : i.NameEnglish;
					return `${i.Code ? i.Code.toString().toLowerCase() : null}${name ? name.toLowerCase() : null}`.indexOf(value.toLowerCase()) != -1;
					// return `${i.Code ? i.Code.toLowerCase() : null}${name ? name.toLowerCase() : null}`.indexOf(value.toLowerCase()) != -1;
				});
				if (this.filteredList.length == 1) this.selected = this.filteredList[0]; else this.selected = {};
			}
		} else {
			this.filteredList = this.selectList;
		}
	}
	clickOnInput() {
		this.filteredList = this.selectList;
		this.isOpen = true;
		setTimeout(() => this.filterInput.nativeElement.focus());
	}
	inputBlur() {
		this.filterInput.nativeElement.value = null;
		this.filterInput.nativeElement.blur();
		setTimeout(() => this.isOpen = false, 150);
	}
	selectItemFromList(selectItem, submetChange = true) {
		if (selectItem) {
			this.selected = selectItem;
			this.selectedText = `${selectItem.Code ? selectItem.Code : ''} ${selectItem.Name || selectItem.NameEnglish ? this.languageService.getLanguageOrDefault() == 'ar' ? selectItem.Name : selectItem.NameEnglish : ''}`;
			this.isOpen = false;
			if (submetChange) this.onChange(this.selected.ID);
			this.getFullObj.emit(this.selected);
			this.inputBlur();
			// this.onTouched();
		}
	}
	removeSelected() {
		this.selected = {};
		this.selectedText = null;
		this.getFullObj.emit({});
		this.onChange(null);
	}
	downArrowDown() {
		if (this.selected.ID) {
			let index = this.filteredList.findIndex(i => i.ID == this.selected.ID);
			if (this.filteredList[index + 1])
				this.selected = this.filteredList[index + 1];
		} else {
			this.selected = this.filteredList[0];
		}
	}
	downArrowUp() {
		if (this.selected.ID) {
			let index = this.filteredList.findIndex(i => i.ID == this.selected.ID);
			if (this.filteredList[index - 1]) this.selected = this.filteredList[index - 1];
		}
	}
	upEnter() {
		if (this.selected.ID) this.selectItemFromList(this.selected);
	}
	writeValue(obj): void {
		// console.log('obj', obj)
		if (obj) {
			if (this.searchService) {
				this.searchService.GetByID(obj).subscribe(res => {
					if (res.Success) {
						if (this.selectList.findIndex(i => i.ID == res.Data.ID) === -1)
							this.selectList.unshift(res.Data)
						this.filteredList = this.selectList;
						this.selectItemFromList(this.selectList.find(i => i.ID == obj), false);
					}
				});
			} else {
				if (this.selectList && this.selectList.length) {
					this.selectItemFromList(this.selectList.find(i => i.ID == obj), false);
				} else {
					this.writeValueID = obj;
				}
			}
		} else {
			this.selected = {};
			this.selectedText = null;
		}
	}
	onChange = (data) => this.writeValue(data);
	registerOnChange(fn: any): void {
		this.onChange = fn;
	}
	onTouched = () => { };
	registerOnTouched(fn: any): void {
		this.onTouched = fn;
		// console.log(this.onTouched.prototype)
	}
	setDisabledState?(isDisabled: boolean): void {
		this.isDisabled = isDisabled
	}
	// updateControl(control, dir): void {
	// console.log(control, dir)
	// 	// dir.viewToModelUpdate(control._pendingValue);
	// 	// if (control._pendingDirty) control.markAsDirty();
	// 	// control.setValue(control._pendingValue, {emitModelToViewChange: false});
	// }
}
import * as moment from 'moment';

export interface DateRange {
  fromDate: Date | string;
  toDate: Date | string;
}

const getCurrentDateRange = (type: 'day' | 'month' | 'year'): DateRange => {
  const fromDate = moment().startOf(type).toDate();
  const toDate = moment().endOf(type).toDate();
  return { fromDate, toDate };
};

export const getCurrentDay = (): DateRange => {
  return getCurrentDateRange('day');
};

export const getCurrentMonth = (): DateRange => {
  return getCurrentDateRange('month');
};

export const getCurrentYear = (): DateRange => {
  return getCurrentDateRange('year');
};

import { Component, Input, OnInit, forwardRef, SimpleChange, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { LanguageService } from "../../services/language.service";
import { NG_VALUE_ACCESSOR, AsyncValidator, ControlValueAccessor } from '@angular/forms';
import { concatAll } from 'rxjs/operators';

@Component({
	selector: 'app-select-get-multi',
	templateUrl: 'select-get-multi.component.html',
	styleUrls: ['select-get-multi.component.css'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			multi: true,
			useExisting: forwardRef(() => SelectGetMultiComponent),
		}
	]
})
export class SelectGetMultiComponent implements ControlValueAccessor, OnInit {
	@Input() dataService;
	@Input() dataSet;
	@Input() label;
	@Input() isRequired: boolean = false;
	@Input() resultkeyName: string;
	@Input() propToBeZero: string;
	@Input() filterBy: any[] = null;
	@Output() getFullObj: EventEmitter<any> = new EventEmitter<any>();
	@ViewChild('filterInput') private filterInput: ElementRef;

	// @Input() formControlName: string;
	filter: string;
	selectLoading: boolean = false;
	selectList: any[] = [];
	selected: any = {};
	selectedText: string;
	isOpen: boolean = false;
	isDisabled: boolean = false;
	writeValueID: number;

	
	constructor(public languageService: LanguageService) { }
	ngOnInit() {

		this.selectLoading = true;
		if (this.dataService) {
			this.dataService.GetList().subscribe(
				res => {
					this.selectLoading = false;
					if (res.Success) {
						res.Data.forEach(item => {
							item[this.resultkeyName] = Object.assign({}, item).ID;
							this.propToBeZero.split(',').forEach(prop => item[prop] = 0);
							item.ID = 0;
						});
						if (this.selectList.length) {
							let propName = this.resultkeyName.slice(0, this.resultkeyName.length - 2);
							this.selectList.forEach(item => {
								if (res.Data.find(thisItem => thisItem[this.resultkeyName] === item[propName].ID))
									res.Data.find(thisItem => thisItem[this.resultkeyName] === item[propName].ID).selected = true;
								item[this.resultkeyName] = item[propName].ID;
								this.propToBeZero.split(',').forEach(prop => item[prop] = 0);
							})
						}
						this.selectList = res.Data;
					}
				}
			);
		} else if (this.dataSet && this.dataSet.length) {
			this.selectLoading = false;
			if (this.filterBy) {
				this.selectList = this.dataSet.filter(i => i[this.filterBy[0]] == this.filterBy[1]);
			} else {
				this.selectList = this.dataSet;
			}
		}
	}
	ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
		this.selectLoading = true;
		if(changes.disabledIDs){
			this.selectLoading = false;
		}
		if (changes.dataSet) {
			this.selectLoading = false;
			this.selectList = changes.dataSet.currentValue;
			if (this.writeValueID)
				this.selectItemFromList(this.selectList.find(i => i.ID == this.writeValueID));
		}
	}
	unSelectAll(){
	
		this.selectList.forEach(item => item.selected = false);
	}
	selectAll(){
		this.selectList.forEach(item => item.selected = true);
	}
	selectItemFromList(selectItem, submetChange = true) {
		if (selectItem) {
			this.selected = selectItem;
			this.selectedText = `${selectItem.Code ? selectItem.Code : ''} ${selectItem.NameArabic || selectItem.NameEnglish ? this.languageService.getLanguageOrDefault() == 'ar' ? selectItem.NameArabic : selectItem.NameEnglish : ''}`;
			this.isOpen = false;
			if (submetChange) this.onChange(this.selected.ID);
			this.getFullObj.emit(this.selected);
			this.inputBlur();
			//this.onTouched();
		}
	}
	inputBlur() {
		this.filterInput.nativeElement.value = null;
		this.filterInput.nativeElement.blur();
		setTimeout(() => this.isOpen = false, 150);
	}
	GetList() {
		this.selectLoading = true;
		this.dataService.GetList().subscribe(
			res => {
				this.selectLoading = false;
				if (res.Success) {
					res.Data.forEach(item => {
						item[this.resultkeyName] = Object.assign({}, item).ID;
						this.propToBeZero.split(',').forEach(prop => item[prop] = 0);
						item.ID = 0;
					});
					if (this.selectList.length) {
						let propName = this.resultkeyName.slice(0, this.resultkeyName.length - 2);
						this.selectList.forEach(item => {
							if (res.Data.find(thisItem => thisItem[this.resultkeyName] === item[propName].ID))
								res.Data.find(thisItem => thisItem[this.resultkeyName] === item[propName].ID).selected = true;
							item[this.resultkeyName] = item[propName].ID;
							this.propToBeZero.split(',').forEach(prop => item[prop] = 0);
						})
					}
					this.selectList = res.Data;
				}
			}
		)
	}
	getUnselectedItems(): any[] {
		let filteredItems;
		let arOrEnFiled;
		let unSelectedItems = this.selectList.filter(item => !item.selected);
		this.languageService.getLanguageOrDefault() == 'ar' ? arOrEnFiled = 'NameArabic' : arOrEnFiled = 'NameEnglish';
		if (this.filter) {
			filteredItems = unSelectedItems.filter(item => item[arOrEnFiled].toLowerCase().indexOf(this.filter.toLowerCase()) != -1);
		} else {
			filteredItems = unSelectedItems;
		}
		return filteredItems;
	}
	getSelectedItems(): any[] {
		return this.selectList.filter(item => item.selected);
	}
	writeValue(objs: any): void {
		if (objs) this.selectList = objs;
		if (!this.selectLoading) this.GetList();

	}
	onChange = (data) => this.writeValue(data);
	registerOnChange(fn: any): void {
		this.onChange = fn;
	}
	selectTouched = () => { };
	registerOnTouched(fn: any): void {
		this.selectTouched = fn;
	}
	setDisabledState?(isDisabled: boolean): void {
		this.isDisabled = isDisabled
	}
}
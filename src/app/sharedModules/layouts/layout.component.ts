import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router, NavigationEnd, RouterEvent, NavigationStart, ActivationStart, RoutesRecognized } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../services/language.service';
import { SubLayoutService, GridActions } from '../../services/sub-layout.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../services/user/auth.service';
import { LocalStorageService } from 'angular-2-local-storage';
declare var require: any;
@Component({
	selector: 'app-layout',
	templateUrl: 'layout.component.html',
	styleUrls: ['layout.component.css']
})
export class LayoutComponent implements OnInit {
	isMenuOpen: boolean = false;
	isSubMenuOpen: boolean = false;
	notifyCount: number;
	moduleRoute: string = this.router.url.split('/')[1];
	subLayoutEvent: Subscription;
	GridActions = GridActions;
	gridAction: any = GridActions.Loaded;
	noSubMenu: boolean = false;
	noSlider: boolean = false;
	noPanel: boolean = false;
	 allpermission: any[] = [];
	 tabactive:string;
	currentResources: any[] = [];
	modules: any[] = [];
	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private subLayout: SubLayoutService,
		public translateService: TranslateService,
		public languageService: LanguageService,
		public authService: AuthService,
		public localStorageService:LocalStorageService
	) { }
	ngOnInit() {
		console.log(this.activatedRoute.snapshot.children[0],"lll")
		this.allpermission=this.localStorageService.get('permissions');
		if(this.allpermission.find(i=>i.id==9)){
			this.tabactive='dashboard_management';
		  }
		  else
		  {
			this.tabactive=this.allpermission[0].route;
		  }
		
		this.activatedRoute.data.subscribe(data => {
		if (data['noSubMenu']) this.noSubMenu = true;
		if (data['noSlider']) this.noSlider = true;
		});
		
		if (this.activatedRoute.children[0] && this.activatedRoute.children[0].snapshot.data['noPanel']) this.noPanel = true; else this.noPanel = false;
		this.router.events.subscribe(e => {
			if (e instanceof NavigationStart) {
				let selectedModule = this.modules.find(i => i.Url == e.url.split('/')[1]);
				if (selectedModule && !selectedModule.loading) selectedModule.loading = true;
			}
			if (e instanceof NavigationEnd) {
				let selectedModule = this.modules.find(i => i.Url == e.url.split('/')[1]);
				if (selectedModule && selectedModule.loading) setTimeout(() => selectedModule.loading = false, 1000);
				if (this.activatedRoute.children[0] && this.activatedRoute.children[0].snapshot.data['noPanel']) this.noPanel = true; else this.noPanel = false;
			}
		})
		this.subLayoutEvent = this.subLayout.on(GridActions.Action).subscribe((gridAction: GridActions) => this.gridAction = gridAction);
		if (this.translateService.currentLang === 'ar') {
			require("style-loader!bootstrap-rtl/dist/css/bootstrap-rtl.min.css");
			require("style-loader!src/assets/css/ar/Style.css");
		} else {
			require("style-loader!src/assets/css/en/Style.css");
		}
	}
	ngOnDestroy() {
		this.subLayoutEvent.unsubscribe();
	}
	logout() {
		this.authService.LogOut();
	}

	activeRoute(route){
		this.tabactive=route;
		console.log('ertghjk',route);
	}

}

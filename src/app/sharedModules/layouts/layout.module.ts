import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Route } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { LanguageService } from '../../services/language.service';
import { NotifyModule } from "../notify/notify.module";
import { LayoutComponent } from './layout.component';
import { LoadingComponent } from './loading/loading.component';
import { AuthService } from '../../services/user/auth.service';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forRoot(),
    BsDropdownModule.forRoot(),
    NotifyModule,
  ],
  declarations: [
    LayoutComponent,
    LoadingComponent
  ],
  exports: [
    LoadingComponent
  ],
  providers: [
   AuthService,
   LanguageService
  ],
})
export class LayoutModule { }

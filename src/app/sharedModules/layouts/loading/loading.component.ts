import { Component, OnInit } from '@angular/core';
import { SubLayoutService, GridActions } from "../../../services/sub-layout.service";
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-loading',
	templateUrl: 'loading.component.html',
	styleUrls: ['loading.component.css'],
})
export class LoadingComponent implements OnInit {
	subLayoutEvent: Subscription;
	GridActions = GridActions;
	gridAction: any = GridActions.Loaded;
	constructor(
		private subLayout: SubLayoutService,
	) { }
	ngOnInit() {
		this.subLayoutEvent = this.subLayout.on(GridActions.Action).subscribe((gridAction: GridActions) => this.gridAction = gridAction);
	}
	ngOnDestroy() {
		this.subLayoutEvent.unsubscribe();
	}
}
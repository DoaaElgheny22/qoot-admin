import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mask',
  templateUrl: './mask.component.html',
  styleUrls: ['./mask.component.css']
})
export class MaskComponent implements OnInit {
  @Input() isRequired: boolean = false;
  @Input() label;
  @Input() nolabel;
  isDisabled: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}

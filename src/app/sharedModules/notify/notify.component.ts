import { Component, OnInit } from '@angular/core';
import { NotifyService, Notify, NotifyTypes } from "../../services/notify.service";
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-notify',
	templateUrl: 'notify.component.html',
	styleUrls: ['notify.component.css'],
})
export class NotifyComponent implements OnInit {

	notifys: Notify[] = [];
	notifyType = NotifyTypes;
	notifyEvent: Subscription;
	constructor(
		private notifyService: NotifyService
	) { }
	pushNotify(notify: Notify) {
		notify._class = 'fadeIn animated';
		notify._close = (index: number) => {
			this.notifys[index]._class = 'fadeOut animated'
			setTimeout(() => {
				this.notifys.splice(index, 1)
			}, 600);
		}
		this.notifys.push(notify);
		
		if (notify.type == NotifyTypes.Success || notify.type == NotifyTypes.Info) {
			setTimeout(() => {
				notify._close(this.notifys.length-1)
			}, 3000);
		}
		if (notify.type == NotifyTypes.Warning || notify.type == NotifyTypes.Error) {
			setTimeout(() => {
				notify._close(this.notifys.length-1)
			}, 5000);
		}
	}
	ngOnInit() {
		this.notifyEvent = this.notifyService.on().subscribe(notify => this.pushNotify(notify));
	}
	ngOnDestroy() {
		this.notifyEvent.unsubscribe();
	}
}
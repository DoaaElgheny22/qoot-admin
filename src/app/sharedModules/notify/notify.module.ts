import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotifyService } from "../../services/notify.service";

import { NotifyComponent } from "./notify.component";

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    NotifyComponent
  ],
  exports: [
    NotifyComponent
  ],
  providers: [
    NotifyService
  ],
})
export class NotifyModule { }

import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { HttpRequest, HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';

@Component({
	selector: 'app-files-upload',
	templateUrl: 'files-upload.component.html',
	styleUrls: ['files-upload.component.css'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			multi: true,
			useExisting: forwardRef(() => FilesUploadComponent),
		}
	]
})
export class FilesUploadComponent implements ControlValueAccessor, OnInit {
	@Input() label;
	@Input() isRequired: boolean = false;
	@Input() isMultiple: boolean = false;
	@Input() acceptedExtensions: string[] = ["png", "gif", "jpg", "pdf", "doc", "docx", "xlsx", "xls", "iso"];
	@Input() AttachmentTypeID: AttachmentType;
	@Input() veiwOnly: boolean = false;
	acceptedExtensionsString: string;
	isDisabled: boolean = false;
	filesStatus = [];
	constructor(
		private http: HttpClient
	) { }
	ngOnInit() {
		this.acceptedExtensionsString = this.acceptedExtensions.reduce((str, ext) => `.${ext},${str}`, '');
	}
	uploadFiles(files) {
		for (var j = 0; j < files.length; j++) {
			const fileStatus = <any>{};
			const formData: FormData = new FormData();
			formData.append("file", files[j], files[j].name);
			fileStatus.Name = files[j].name;
			let spiletedName = files[j].name.split('.')
			fileStatus.Extension = spiletedName[spiletedName.length - 1];
			fileStatus.Success = true;
			if (this.acceptedExtensions.indexOf(fileStatus.Extension.toLowerCase()) > -1) {
				const progress = new Subject<number>();
				fileStatus.uploudRequest = this.http.request(new HttpRequest('POST', `${environment.api_url}/File/Upload`, formData, { reportProgress: true })).subscribe(
					event => {
						if (event.type === HttpEventType.UploadProgress) {
							progress.next(Math.round(100 * event.loaded / event.total));
						} else if (event instanceof HttpResponse) {
							progress.complete();
						}
						if (event.type === HttpEventType.Response) {
							if (event.body['Success']) {
								Object.assign(fileStatus, event.body['Data'][0])// extent object
								if (this.isMultiple) {
									this.onChange(this.filesStatus);
								} else {
									this.onChange(event.body['Data'][0].Path);
								}
							} else {
								fileStatus.Message = event.body['Message'];
								fileStatus.Success = event.body['Success'];
							}
						}
					},
					error => {
						fileStatus.Message = error.message;
						fileStatus.Success = false;
					}
				);
				progress.subscribe(process => fileStatus.progress = process);
				if (this.isMultiple) {
					fileStatus.AttachmentTypeID = this.AttachmentTypeID;
					fileStatus.DocumentID = 0;
					this.filesStatus.push(fileStatus);
				} else {
					this.filesStatus[0] = fileStatus;
				}
			}
		}
	}
	cancelUpload(fileName: string) {
		const thisFile = this.filesStatus.find(file => file.Name == fileName);
		thisFile.uploudRequest.unsubscribe();
		this.filesStatus.splice(this.filesStatus.indexOf(thisFile), 1);
	}
	deleteUpload(fileName: string) {
		const thisFile = this.filesStatus.find(file => file.Name == fileName);
		this.filesStatus.splice(this.filesStatus.indexOf(thisFile), 1);
		if (this.isMultiple) {
			this.onChange(this.filesStatus.filter(i=>i.Name != fileName));
		} else {
			this.onChange(null);
		}
	}
	spliteFileName(fileName: string): string {
		let splitedString: string;
		if (fileName && fileName.length > 35) {
			splitedString = fileName.replace(fileName.slice(12, fileName.length - 12), '...');
		} else {
			splitedString = fileName;
		}
		return splitedString;
	}
	openLink(url) {
		let dualScreenLeft = window.screenLeft != undefined && window.screenLeft;
		let dualScreenTop = window.screenTop != undefined && window.screenTop;
		let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
		let w = 850;
		let h = 500
		let left = ((width / 2) - (w / 2)) + dualScreenLeft;
		let top = ((height / 2) - (h / 2)) + dualScreenTop;
		// let thisWindow =
		window.open(url, 'thisWindow', 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
		// thisWindow['print']();
	}
	writeValue(obj: any): void {
		// console.log(obj)
		if (obj) {
			if (this.isMultiple) {
				if (obj.length) {
					obj.forEach(file => {
						let fileStatus = <any>{};
						fileStatus.Name = file.Name;
						fileStatus.ID = file.ID;
						let spiletedName = file.Path.split('.');
						fileStatus.Extension = spiletedName[spiletedName.length - 1];
						fileStatus.DocumentID = file.DocumentID;
						fileStatus.AttachmentTypeID = file.AttachmentType.ID;
						fileStatus.Success = true;
						fileStatus.TempUrl = file.Path;
						fileStatus.Path = file.Path;
						this.filesStatus.push(fileStatus);
					});
				}
			} else {
				let fileName = obj.split('/')[obj.split('/').length - 1].split('[^STOP^]')[0];
				this.filesStatus[0] = {
					// Path: obj,
					Extension: fileName.split('.')[fileName.split('.').length - 1],
					Name: fileName,
					TempUrl: obj
				};
			}
		}
	}
	onChange = (data) => this.writeValue(data);
	registerOnChange(fn: any): void {
		this.onChange = fn;
	}
	onTouched = () => { };
	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}
	setDisabledState?(isDisabled: boolean): void {
		this.isDisabled = isDisabled
	}
}
export enum AttachmentType {
	StoreRequest = 1,
	PurchaseOrder = 2,
	Shipment = 3,
	Document = 4
}
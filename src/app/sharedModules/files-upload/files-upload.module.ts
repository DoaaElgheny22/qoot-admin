import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { FilesUploadService } from "../../services/files-upload.service";
import { FilesUploadComponent } from './files-upload.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: null,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    FilesUploadComponent
  ],
  exports: [FilesUploadComponent],
  providers: [
    FilesUploadService
  ],
})
export class FilesUploadModule { }

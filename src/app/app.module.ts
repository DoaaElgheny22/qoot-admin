import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule,Route, Router } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpLoaderFactory } from './pages/home/home-routing.module';
import { AppRoutingModule } from './sharedModules/routing.module';
import { AuthService } from './services/user/auth.service';
import { WebApiService } from './services/webApi.service';
import { NotifyService } from './services/notify.service';
import { LocalStorageModule } from 'angular-2-local-storage';
import {DataTablesModule} from 'angular-datatables';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { AuthGuard } from './services/user/guards/auth.guard';
import { LanguageService } from './services/language.service';
// AoT requires an exported function for factories
export function httpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    FilterPipeModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,

        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    DataTablesModule,
    LocalStorageModule.forRoot({
      prefix: 'qootSystem',
      storageType: 'localStorage'
    }),
  ],
  providers: [AuthService,WebApiService,NotifyService, LanguageService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

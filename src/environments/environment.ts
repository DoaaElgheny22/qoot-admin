// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  defaultDateFormat: 'DD-MM-YYYY',
  defaultPageSize: 20,
  api_url:'https://backend-qoot.qoot.online/api/Admin',
  api_url_main:'https://backend-qoot.qoot.online/api',
  api_url_rep:'https://backend-qoot.qoot.online/',
  api_img:'https://backend-qoot.qoot.online/',
  defaultLanguage: 'ar',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

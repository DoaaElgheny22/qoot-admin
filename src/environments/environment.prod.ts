export const environment = {
  production: true,
  defaultDateFormat: 'DD-MM-YYYY',
  api_url:'https://backend-qoot.qoot.online/api/Admin',
  api_url_main:'https://backend-qoot.qoot.online/api',
  api_url_rep:'https://backend-qoot.qoot.online/',
  api_img:'https://backend-qoot.qoot.online/',
  defaultLanguage: 'ar',
  defaultPageSize:20

};
